///
/// @package phosim
/// @file main.cpp
/// @brief main for atmosphere program
///
/// @brief Created by
/// @author John R. Peterson (Purdue)
///
/// @brief Modified by
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include "atmosphere/atmosphere.h"

int main(void) {
    // Default values.
    int obshistid = 9999;
    double outerx = 500000.0;
    double pix = 256.0; // pixel size in cm
    int telconfig = 0;
    float constrainseeing = -1;

    double seeing;
    double constrainairglow = -1.0;
    char tempstring[4096];
    FILE *fptr;
    char outputfilename[4096];
    double zenith, altitude;
    float monthnum;
    long seed = 0;
    double tai = 0.0;

    int atmospheremode = 1;
    int opticsonlymode = 0;
    int turbulencemode = 1;
    int opacitymode = 1;
    long atmospheric_dispersion = 1;

    Atmosphere atmosphere;

    atmosphere.numlevel = 7;
    atmosphere.constrainclouds = -1.0;
    static int maxlevel = 100;
    std::vector<int> cloudscreen(maxlevel, 0);
    cloudscreen[1] = 1;
    cloudscreen[2] = 1;
    std::vector<float> outerscale(maxlevel, -1);


    // Set some default values.
    atmosphere.datadir = "../data";
    atmosphere.instrdir = "../data/lsst";
    std::string outputdir(".");

    std::cout << "------------------------------------------------------------------------------------------" << std::endl
    << "Atmosphere Creator" << std::endl
    << "------------------------------------------------------------------------------------------" << std::endl;

    // Read parameters from stdin.
    readText pars(std::cin);
    for (size_t t(0); t < pars.getSize(); t++) {
        std::string line(pars[t]);
        std::istringstream iss(line);
        std::string keyName;
        iss >> keyName;
        readText::get(line, "outputdir", outputdir);
        readText::get(line, "instrdir", atmosphere.instrdir);
        readText::get(line, "datadir", atmosphere.datadir);
        readText::get(line, "numlevel", atmosphere.numlevel);
        readText::get(line, "telconfig", telconfig);
        readText::get(line, "obshistid", obshistid);
        readText::get(line, "outerx", outerx);
        readText::get(line, "cloudscreen", cloudscreen);
        readText::get(line, "outerscale", outerscale);
        readText::get(line, "obsseed", seed);
        readText::get(line, "monthnum", monthnum);
        readText::get(line, "tai", tai);
        readText::get(line, "atmospheremode", atmospheremode);
        readText::get(line, "opticsonlymode", opticsonlymode);
        readText::get(line, "atmosphericdispersion", atmospheric_dispersion);

        // setting numlayers to 1 is the best I can do for now, without being too dangerous. 0 layers is
        // dangerous because other parts of the code will try to read atmospherefiles that do not exist.
        // Although we could hide the output from the user to avoid confusion (CB)
        if (keyName == "cleareverything" || opticsonlymode == 1 || atmospheremode == 0) atmosphere.numlevel = 1;
        if (keyName == "clearturbulence") turbulencemode = 0;
        if (keyName == "clearopacity") opacitymode = 0;
        if (turbulencemode == 0 && atmospheric_dispersion == 0.0 && opacitymode == 0) atmosphere.numlevel = 1;

        if (readText::getKey(line, "constrainseeing", seeing)) constrainseeing = seeing/2.35482;
        if (readText::getKey(line, "altitude", altitude)) zenith = 90 - altitude;
        readText::get(line, "zenith", zenith);
        readText::getKey(line, "constrainclouds", atmosphere.constrainclouds);
        readText::getKey(line, "constrainairglow", constrainairglow);

    }
    atmosphere.osests.reserve(atmosphere.numlevel);
    atmosphere.altitudes.reserve(atmosphere.numlevel);
    atmosphere.jests.reserve(atmosphere.numlevel);
    
    std:: string sss;
    sss = atmosphere.instrdir + "/location.txt";
    std::ifstream inStream(sss.c_str());
    if (inStream) {
        readText locationPars(atmosphere.instrdir + "/location.txt");
        for (size_t t(0); t<locationPars.getSize(); t++) {
            std::string line(locationPars[t]);
            readText::get(line, "groundlevel", atmosphere.groundlevel);
        }
    }
    
    sprintf(outputfilename, "%s/atmosphere_%d.pars", outputdir.c_str(), obshistid);

    atmosphere.createAtmosphere(monthnum, constrainseeing, outputfilename, cloudscreen, seed, tai);

    /// overwrite outer scales if they have been provided.
    for (int m(0); m < atmosphere.numlevel; m++) {
        if (outerscale[m] >= 0) atmosphere.osests[m] = outerscale[m];
    }

    fptr = fopen(outputfilename, "a+");
    for (int i = 0; i < atmosphere.numlevel; i++) {
        printf("Creating layer %d.\n", i);
        double outers = atmosphere.osests[i]*100.0;
        sprintf(tempstring, "%s/atmospherescreen_%d_%d", outputdir.c_str(), obshistid, i);
        atmosphere.turb2d(seed*10 + i, seeing, outerx, outers, zenith, 0.5, tempstring);
        fprintf(fptr, "atmospherefile %d atmospherescreen_%d_%d\n", i, obshistid, i);
    }
    for (int i = 0; i < atmosphere.numlevel; i++) {
        if (cloudscreen[i]) {
            double height = (atmosphere.altitudes[i] - atmosphere.groundlevel)/1000.0;
            sprintf(tempstring, "%s/cloudscreen_%d_%d", outputdir.c_str(), obshistid, i);
            atmosphere.cloud(seed*10 + i, height, pix, tempstring);
            fprintf(fptr, "cloudfile %d cloudscreen_%d_%d\n", i, obshistid, i);
        }
    }

    sprintf(tempstring, "%s/airglowscreen_%d", outputdir.c_str(), obshistid);
    atmosphere.airglow(seed*10, tempstring);

    atmosphere.random.setSeed32(seed);
    atmosphere.random.unwind(10000);
        // fprintf(fptr, "zenith_v %.3f\n", 22.08 + 0.9*atmosphere.random.uniform());
    if (constrainairglow > 0.0) {
        fprintf(fptr, "airglowintensity %.3f\n", constrainairglow);
    } else {
        // fprintf(fptr, "airglowintensity %.3f\n", (21.7 + 0.7*atmosphere.random.uniform())/cos(zenith*M_PI/180.0));
        fprintf(fptr, "airglowintensity %.3f\n",
                (21.7 + 0.2*atmosphere.random.normalCorrel(tai,500.0/(24.0*3600.))
                 + 2.5*log10(cos(zenith*M_PI/180.0))
                 + (0.35 + 0.35*sin(2*M_PI*(tai-54466.0)/(365.24*11.0) - M_PI/2.0))));
     }
    fclose(fptr);

    return(0);
}

///
/// @package phosim
/// @file sourceloop.cpp
/// @brief source loop
///
/// @brief Created by:
/// @author John R. Peterson (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///
#include <pthread.h>
#include <unistd.h>

int Image::sourceLoop() {

    char tempstring[4096];
    long sourceCounter;
    long surfaceLimit;
    pthread_t *thread;
    thread_args *args;

    //    OPTIMIZATION INITIALIZATION
    thread = (pthread_t*)malloc(numthread*sizeof(pthread_t));
    args = (thread_args*)malloc(numthread*sizeof(thread_args));
    openthread = (int*)malloc(numthread*sizeof(int));
    for (int i = 0; i < numthread; i++) openthread[i] = 0;
    surfaceLimit = natmospherefile*2 + nsurf*2 + 2;
    state.dynamicTransmission = static_cast<std::atomic<double>*>(malloc(surfaceLimit*(MAX_WAVELENGTH-MIN_WAVELENGTH+1)*sizeof(std::atomic<double>)));
    for (int i = 0; i < MAX_WAVELENGTH-MIN_WAVELENGTH+1; i++) {
        for (int j = 0; j < surfaceLimit; j++) {
            state.dynamicTransmission[i*surfaceLimit + j] = -1.0;
        }
    }
    if (checkpointcount != 0) readCheckpoint(checkpointcount);

    //    LOGGING INITIALIZATION
    if (eventfile) {
        eventFitsFileName = std::string(this->outputfilename);
        eventFitsFileName = eventFitsFileName.replace(eventFitsFileName.find("_e_"), 3, "_r_") + ".fits";
        state.pEventLogging = new EventFile((int)(nphot*100), outputdir, eventFitsFileName);
    } else {
        state.pEventLogging = NULL;
    }
    pGrating = new Grating();
    if (throughputfile) initThroughput(&state.throughputLog, nsurf);
    counterInit(&state.counterLog);
    counterClear(&state.globalLog);
    // state.cx = 0.0;
    // state.cy = 0.0;
    // state.cz = 0.0;
    // state.r0 = 0.0;
    // state.epR = 0.0;
    // pthread_mutex_init(&lock.lock1, NULL);
    // pthread_mutex_init(&lock.lock2, NULL);
    // pthread_mutex_init(&lock.lock3, NULL);
    if (opdfile == 1) pthread_mutex_init(&lock.lock4, NULL);
    if (opdfile == 1) pthread_mutex_init(&lock.lock5, NULL);
    if (opdfile == 1) pthread_mutex_init(&lock.lock6, NULL);
    // pthread_mutex_init(&lock.lock7, NULL);
    pthread_mutex_init(&lock.lock8, NULL);
    if (opdfile == 1) pthread_cond_init(&lock.cond, NULL);
    // pthread_cond_init(&lock.cond2, NULL);
    openthreads = 0;
    long subsource = 0;


    //    SOURCE TYPE LOOP
    for (int sourceType = 546; sourceType >= 0; sourceType--) {

        sourceCounter = 0;

        if (static_cast<int>(round(sourceType*checkpointtotal/546)) == checkpointcount) {

            //    SOURCE LOOP
            // long subsource = 0;

            for (long source = 0; source < nsource; source++) {

                        if ((sourceType >= 0   && sourceType <=  99 && sources.type[source] == 0 && (source%100) == sourceType) ||
                            (sourceType >= 100 && sourceType <= 199 && sources.type[source] == 1 && (source%100) == (sourceType - 100)) ||
                            (sourceType >= 200 && sourceType <= 299 && sources.type[source] == 2 && (source%100) == (sourceType - 200)) ||
                            (sourceType >= 300 && sourceType <= 399 && sources.type[source] == 3 && (source%100) == (sourceType - 300)) ||
                            (sourceType >= 400 && sourceType <= 499 && sources.type[source] == 4 && (source%100) == (sourceType - 400)) ||
                            (sourceType == 500 && sources.type[source] == 5 && sources.mag[source] > 32.25) ||
                            (sourceType >= 501 && sourceType <= 545 && sources.type[source] == 5 &&
                             sources.mag[source] >  (282.25 - static_cast<double>(sourceType)/2.0) &&
                             sources.mag[source] <= (282.75 - static_cast<double>(sourceType)/2.0))
                            || (sourceType == 546 && sources.type[source] == 5 && sources.mag[source] <= 9.75)) {

                            while (openthreads >= numthread*sourceperthread) {
                                // printf("%ld %ld\n",openthreads,numthread);
                                usleep(100*sourceperthread);
                            }
                            pthread_mutex_lock(&lock.lock8);
                            long bestthread = sourceperthread + 1;
                            for (int i = 0; i < numthread; i++) {
                                if ((openthread[i] < sourceperthread) && (openthread[i] < bestthread)) {
                                    subsource = i;
                                    bestthread = openthread[i];
                                }
                            }
                            openthread[subsource] += 1;
                            openthreads++;
                            // printf("Thread %ld started for source %ld\n",subsource,source);

                            sourceCounter++;
                            args[subsource].ssource[openthread[subsource]-1] = source;
                            // printf("%ld %d\n",subsource,openthread[subsource]);
                            if (openthread[subsource] == sourceperthread) {
                                args[subsource].instance = this;
                                args[subsource].thread = subsource;
                                args[subsource].runthread = sourceperthread;
                                if (opdfile == 1) {
                                    if ((source % numthread) == 0) {
                                        if (source < static_cast<long>(floor(nsource/numthread))*numthread) {
                                            remain = numthread;
                                        } else {
                                            remain = nsource - static_cast<long>(floor(nsource/numthread))*numthread;
                                        }
                                    }
                                }
                                pthread_create(&thread[subsource], NULL, &Image::threadFunction, &args[subsource]);
                                pthread_detach(thread[subsource]);
                            }
                            pthread_mutex_unlock(&lock.lock8);

                        }

            }
        }
        if (sourceType >= 0  && sourceType < 100)  sprintf(tempstring, "Dome Light     %3d%% ",(100-sourceType));
        if (sourceType >= 100 && sourceType < 200) sprintf(tempstring, "Airglow        %3d%% ",(200-sourceType));
        if (sourceType >= 200 && sourceType < 300) sprintf(tempstring, "Moon-Rayleigh  %3d%% ",(300-sourceType));
        if (sourceType >= 300 && sourceType < 400) sprintf(tempstring, "Moon-Mie       %3d%% ",(400-sourceType));
        if (sourceType >= 400 && sourceType < 500) sprintf(tempstring, "Zodiacal       %3d%% ",(500-sourceType));
        if (sourceType == 500)                     sprintf(tempstring, "Astro Object m>32.0 ");
        if (sourceType >= 501 && sourceType < 546) sprintf(tempstring, "Astro Object m=%4.1f ", 282.5 - static_cast<double>(sourceType)/2.0);
        if (sourceType == 546)                     sprintf(tempstring, "Astro Object m<10.0 ");
        if (sourceCounter > 0) counterCheck(&state.counterLog, sourceCounter, tempstring);
    }
    for (int i=0; i<numthread; i++) {
        if (openthread[i] != sourceperthread && openthread[i] != 0) {
            args[i].instance = this;
            args[i].thread = i;
            args[i].runthread = openthread[i];
            // if (opdfile == 1) {
            //     if ((source % numthread) == 0) {
            //         if (source < static_cast<long>(floor(nsource/numthread))*numthread) {
            //             remain = numthread;
            //         } else {
            //             remain = nsource - static_cast<long>(floor(nsource/numthread))*numthread;
            //         }
            //     }
            // }
            pthread_create(&thread[i], NULL, &Image::threadFunction, &args[i]);
            pthread_detach(thread[i]);
        }
    }
    while (openthreads != 0) {
        usleep(100*sourceperthread);
    }


    // COSMIC RAYS
    long long ray;
    long long detRay;
    if (checkpointcount == checkpointtotal) {
        if (backgroundMode > 0) {
            detRay = 0;
            ray = 0;
            cosmicRays(&detRay);
            if (detRay > 0) {
                for (long i = 0; i < detRay; i++) {
                    countGood(&state.counterLog, 1, &ray);
                }
                sprintf(tempstring, "Cosmic Rays         ");
                counterCheck(&state.counterLog, detRay, tempstring);
            }
        }
    }

    // OUTPUT DATA
    if (checkpointcount == checkpointtotal) writeImageFile();
    if (opdfile) writeOPD();
    if (checkpointcount !=  checkpointtotal) writeCheckpoint(checkpointcount);
    if (centroidfile) writeCentroidFile(outputdir, outputfilename, sourcePhoton, sourceXpos, sourceYpos, sources.id, nsource);
    if (throughputfile) writeThroughputFile(outputdir, outputfilename, &state.throughputLog, nsurf);
    if (eventfile) state.pEventLogging->eventFileClose();
    return(0);


}

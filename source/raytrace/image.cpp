///
/// @package phosim
/// @file image.cpp
/// @brief image class
///
/// @brief Created by:
/// @author John R. Peterson (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <complex>

#include "constants.h"
#include "ancillary/random.h"
#include "helpers.h"
#include "image.h"

#include "atmospheresetup.cpp"
#include "telescopesetup.cpp"
#include "photonmanipulate.cpp"
#include "photonoptimization.cpp"
#include "photonloop.cpp"
#include "sourceloop.cpp"
#include "cosmicrays.cpp"
#include "../ancillary/fits.h"

void Image::writeImageFile () {

    int status = 0;
    char tempstring[4096], tempstring2[4096], line[4096];
    fitsfile *fptr = NULL;
    FILE *indafile;

    std::string filename = "!" + outputdir + "/" + outputfilename + ".fits.gz";
    fitsCreateImage(&fptr, filename.c_str());

    fitsWriteKey(fptr, "CTYPE1", "RA---TAN", "");
    fitsWriteKey(fptr, "CRPIX1", -centerx/pixsize + pixelsx/2, "");
    fitsWriteKey(fptr, "CRVAL1", pra/DEGREE, "");
    fitsWriteKey(fptr, "CTYPE2", "DEC--TAN", "");
    fitsWriteKey(fptr, "CRPIX2", -centery/pixsize + pixelsy/2, "");
    fitsWriteKey(fptr, "CRVAL2", pdec/DEGREE, "");
    fitsWriteKey(fptr, "CD1_1", pixsize/platescale*cos(rotatez), "");
    fitsWriteKey(fptr, "CD1_2", pixsize/platescale*sin(rotatez), "");
    fitsWriteKey(fptr, "CD2_1", -pixsize/platescale*sin(rotatez), "");
    fitsWriteKey(fptr, "CD2_2", pixsize/platescale*cos(rotatez), "");
    fitsWriteKey(fptr, "RADESYS", "ICRS", "");
    fitsWriteKey(fptr, "EQUINOX", 2000.0, "");
    header(fptr);
    fitsWriteKey(fptr, "CREATOR", "PHOSIM", "");

    sprintf(tempstring, "%s/version", bindir.c_str());
    indafile = fopen(tempstring, "r");
    fgets(line, 4096, indafile);
    sscanf(line, "%s %s", tempstring, tempstring2);

    fitsWriteKey(fptr, "VERSION", tempstring2, "");

    fgets(line, 4096, indafile);
    sscanf(line, "%s", tempstring2);
    fclose(indafile);

    fitsWriteKey(fptr, "BRANCH", tempstring2, "");

    if (date) fits_write_date(fptr, &status);

    if (variableMode != 3)
    {

    }

    if (variableMode == 3)
    {
        for (long i = 0; i < chip.nampx; i++) {
            for (long j = 0; j < chip.nampy; j++) {
                *(state.focal_plane_fl + chip.nampx*j + i) =
                    static_cast<float>(*(state.auxillary_focal_plane + chip.nampx*j + i));
            }
        }
    }
    else{
        for (long i = 0; i < chip.nampx; i++) {
            for (long j = 0; j < chip.nampy; j++) {
                *(state.focal_plane_fl + chip.nampx*j + i) =
                    static_cast<float>(*(state.focal_plane + chip.nampx*j + i));
            }
        }
    }
    fitsWriteImage(fptr, chip.nampx, chip.nampy, state.focal_plane_fl);

    Uint32 z = 0, w = 0;
    random.getSeed(&z, &w);

}

void Image::writeOPD () {

    int status;
    char tempstring[4096];
    long naxesa[2];
    fitsfile *faptr;

    status = 0;
    double chiefRayOPL = 0.0;
    long idx;
    long jdx;

    for (long k = 0; k < nsource; k++) {
        jdx = OPD_SCREEN_SIZE*OPD_SCREEN_SIZE*k;
        idx = jdx + (OPD_SCREEN_SIZE/2)*OPD_SCREEN_SIZE + (OPD_SCREEN_SIZE/2);
        chiefRayOPL = state.opd[idx]/state.opdcount[idx];

        for (long i = 0; i < OPD_SCREEN_SIZE; i++) {
            for (long j = 0; j < OPD_SCREEN_SIZE; j++) {
                idx = jdx + i*OPD_SCREEN_SIZE + j;
                if (state.opdcount[idx] > 0) {
                    state.opd[idx] = (state.opd[idx]/state.opdcount[idx] - chiefRayOPL)*1000.0;
                } else {
                    state.opd[idx] = 0.0;
                }
            }
        }


        sprintf(tempstring, "!%s/opd_%ld_%ld.fits.gz", outputdir.c_str(), obshistid, k);
        fits_create_file(&faptr, tempstring, &status);
        naxesa[0] = 1;
        naxesa[1] = 1;
        fits_create_img(faptr, DOUBLE_IMG, 2, naxesa, &status);
        naxesa[0] = OPD_SCREEN_SIZE;
        naxesa[1] = OPD_SCREEN_SIZE;
        fits_update_key(faptr, TLONG, (char*)"NAXIS1", &naxesa[0], NULL, &status);
        fits_update_key(faptr, TLONG, (char*)"NAXIS2", &naxesa[1], NULL, &status);
        fitsWriteKey(faptr, "UNITS", "MICRONS", "");
        fits_write_img(faptr, TDOUBLE, 1, OPD_SCREEN_SIZE*OPD_SCREEN_SIZE, state.opd + jdx, &status);
        fits_close_file(faptr, &status);
    }

}

void Image::writeCheckpoint(int checkpointcount) {

    fitsfile *faptr;
    long naxes[2];
    int status;
    double *tempDynamicTransmission;
    Uint32 z = 0, w = 0;

    random.getSeed(&z, &w);

    tempDynamicTransmission = static_cast<double*>(calloc((natmospherefile*2 + nsurf*2 + 2)*(MAX_WAVELENGTH - MIN_WAVELENGTH + 1), sizeof(double)));
    for (int i = 0; i < MAX_WAVELENGTH - MIN_WAVELENGTH + 1; i++) {
        for (int j = 0; j < (natmospherefile*2 + nsurf*2 + 2); j++) {
            tempDynamicTransmission[i*(natmospherefile*2 + nsurf*2 + 2) + j] =
                state.dynamicTransmission[i*(natmospherefile*2 + nsurf*2 + 2) + j];
        }
    }

    status = 0;
    std::ostringstream filename;
    filename << "!"<< outputdir << "/" << outputfilename << "_ckptdt_" << checkpointcount << ".fits.gz";
    fits_create_file(&faptr, filename.str().c_str(), &status);
    naxes[0] = 1;
    naxes[1] = 1;
    fits_create_img(faptr, DOUBLE_IMG, 2, naxes, &status);
    naxes[0] = MAX_WAVELENGTH - MIN_WAVELENGTH + 1;
    naxes[1] = natmospherefile*2 + nsurf*2 + 2;
    fits_update_key(faptr, TLONG, (char*)"NAXIS1", &naxes[0], NULL, &status);
    fits_update_key(faptr, TLONG, (char*)"NAXIS2", &naxes[1], NULL, &status);
    fits_update_key (faptr, TUINT, (char*)"M_Z", &z, NULL, &status);
    fits_update_key (faptr, TUINT, (char*)"M_W", &w, NULL, &status);
    fits_write_img(faptr, TDOUBLE, 1, (natmospherefile*2 + nsurf*2 + 2)*(MAX_WAVELENGTH - MIN_WAVELENGTH + 1), tempDynamicTransmission, &status);
    fits_close_file(faptr, &status);

    free(tempDynamicTransmission);

    filename.str("");
    filename << "!"<< outputdir << "/" << outputfilename << "_ckptfp_" << checkpointcount << ".fits.gz";
    fits_create_file(&faptr, filename.str().c_str(), &status);
    naxes[0] = 1;
    naxes[1] = 1;
    fits_create_img(faptr, FLOAT_IMG, 2, naxes, &status);
    naxes[0] = chip.nampx;
    naxes[1] = chip.nampy;
    fits_update_key(faptr, TLONG, (char*)"NAXIS1", &naxes[0], NULL, &status);
    fits_update_key(faptr, TLONG, (char*)"NAXIS2", &naxes[1], NULL, &status);
    fits_write_img(faptr, TFLOAT, 1, chip.nampx*chip.nampy, state.focal_plane, &status);
    fits_close_file(faptr, &status);

}

void Image::readCheckpoint(int checkpointcount) {

    fitsfile *faptr;
    long naxes[2];
    int nfound;
    int anynull;
    float nullval;
    int status;
    double *tempDynamicTransmission;
    Uint32 z = 0, w = 0;

    tempDynamicTransmission = static_cast<double*>(calloc((natmospherefile*2 + nsurf*2 + 2)*(MAX_WAVELENGTH - MIN_WAVELENGTH + 1), sizeof(double)));

    std::ostringstream filename;
    filename << outputdir << "/" << outputfilename << "_ckptdt_" << checkpointcount - 1 << ".fits.gz";
    status = 0;
    if (fits_open_file(&faptr, filename.str().c_str(), READONLY, &status)) {
        printf("Error opening %s\n", filename.str().c_str());
        exit(1);
    }
    fits_read_keys_lng(faptr, (char*)"NAXIS", 1, 2, naxes, &nfound, &status);
    fits_read_key(faptr, TUINT, "M_Z", &z, NULL, &status);
    fits_read_key(faptr, TUINT, "M_W", &w, NULL, &status);
    fits_read_img(faptr, TDOUBLE, 1, naxes[0]*naxes[1], &nullval, tempDynamicTransmission, &anynull, &status);
    fits_close_file(faptr, &status);
    random.setSeed(z, w);

    for (int i = 0; i < MAX_WAVELENGTH - MIN_WAVELENGTH + 1; i++) {
        for (int j = 0; j < (natmospherefile*2 + nsurf*2 + 2); j++) {
            state.dynamicTransmission[i*(natmospherefile*2 + nsurf*2 + 2) + j] =
                tempDynamicTransmission[i*(natmospherefile*2 + nsurf*2 + 2) + j];
        }
    }

    free(tempDynamicTransmission);


    filename.str("");
    filename << outputdir << "/" << outputfilename << "_ckptfp_" << checkpointcount - 1 << ".fits.gz";
    status = 0;
    if (fits_open_file(&faptr, filename.str().c_str(), READONLY, &status)) {
        printf("Error opening %s\n", filename.str().c_str());
        exit(1);
    }
    fits_read_keys_lng(faptr, (char*)"NAXIS", 1, 2, naxes, &nfound, &status);
    fits_read_img(faptr, TFLOAT, 1, naxes[0]*naxes[1], &nullval, state.focal_plane, &anynull, &status);
    fits_close_file(faptr, &status);

}


void Image::cleanup () {


}

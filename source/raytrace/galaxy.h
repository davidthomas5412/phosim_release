///
/// @package phosim
/// @file galaxy.h
/// @brief galaxy header
///
/// @brief Created by:
/// @author Suzanne Lorenz (Purdue)
///
/// @brief Modified by:
/// @author John R. Peterson (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include "ancillary/random.h"

class Galaxy {

 public:

    double **fAll;
    double **cAll;
    double *dAll;
    double **fAll2d;
    double **cAll2d;
    double *dAll2d;
    Random random;

    int sersic(double a, double b, double c, double alpha, double beta,
               double n, double *xOut, double *yOut);

    double sampleSersic(char*);

    int sersic2d(double a, double b, double beta, double n,
                 double *xOut, double *yOut);

    double sampleSersic2d(char*);

};

//Gen 35 star X,Y coords in FOV.
void Bo35Stars()
{
  double PI=3.1415926535897932384626433;
  vector< double > radii;
  radii.push_back( 0.379);
  radii.push_back( 0.841);
  radii.push_back( 1.237);
  radii.push_back( 1.535);
  radii.push_back( 1.708);

  int numRadii=radii.size();
  double xDeg=0;
  double yDeg=0;
  cout<<xDeg<<" "<<yDeg<<endl;

  for (int j=0; j<numRadii;j++) {
    double radius=radii.at(j);
    
    for (int i=0; i<6; i++ ) {
      double angle=i*60.0;
      xDeg=cos(angle*PI/180.)*radius;
      yDeg=sin(angle*PI/180.)*radius;
      if (abs(yDeg)<0.0000001){
      	yDeg=0.0;
      }
      cout<<std::setprecision(8)<<xDeg<<" "<<std::setprecision(8)<<yDeg
	  <<endl;
    }
  }
  xDeg=1.185;
  yDeg=1.185;
  cout<<std::setprecision(8)<<xDeg<<" "<<std::setprecision(8)<<yDeg<<endl;
  xDeg=-1.185;
  yDeg=1.185;
  cout<<std::setprecision(8)<<xDeg<<" "<<std::setprecision(8)<<yDeg<<endl;
  xDeg=1.185;
  yDeg=-1.185;
  cout<<std::setprecision(8)<<xDeg<<" "<<std::setprecision(8)<<yDeg<<endl;
  xDeg=-1.185;
  yDeg=-1.185;
  cout<<std::setprecision(8)<<xDeg<<" "<<std::setprecision(8)<<yDeg<<endl;
}
    


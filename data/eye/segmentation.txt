# device segmentation data
# 
# (a): device name
# (b): number of amplifiers
# (c): pixels x
# (d): pixels y
#  
# (1): amplifier name
# (2-5): x low, x high, y low, y high
# (6): serialread
# (7): parallelread
# (8-9): gain, % variation
# (10-11): bias level, % variation
# (12-13): readnoise , % variation
# (14-15): dark current, % variation
# (16-19): parallel prescan, serial overscan, serial prescan, parallel overscan (pixel)
# (20): hot pixel rate
# (21): hot column rate
# (22-35): crosstalk
# 
#  (a)    (b)       (c)         (d)
#  (1)           (2)  (3)    (4)  (5) (6) (7)(8 9) (10 11)(12 13)(14 15) (16-19)    (20)      (21)      (22-37)
chip	   1	   1024         1024
amplifier     0  1023     0   1023  0 0 1.0 0.0 0.0 0.0 0.0 0.0 0.00 0.0  0  0  0  0 0.00000000 0.00000000 0 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00 +0.000e+00

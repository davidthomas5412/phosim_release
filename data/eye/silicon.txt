# silicon properties (being tested)
edgeSurfaceCharge -5e9
treeRingPeriod 375.0
treeRingAmplitude 0.03
treeRingRatio 7.5
treeRingVariation 75.0
treeRingDecay 15000.0
deadLayerDepth 0.75
deadLayerWidth 3000.0
deadLayerHeight 500.0
deadLayerXoverlap -200.0
deadLayerYoverlap -200.0
deadLayerXinit 0 0.0
deadLayerXinit 1 300.0
deadLayerYinit 0 0.0
deadLayerYinit 1 300.0
deadLayerAngle 0 10.0
deadLayerAngle 1 10.0
pixelVarX 0.003
pixelVarY 0.002
wellDepth 100000
nbulk 1e12
nf -5e15
nb 0.0
sf 0.01
sb 0.0025
spaceChargeShield 0.12
spaceChargeSpreadX 4.0
spaceChargeSpreadY 1.0
chargeStopCharge 1000
siliconType -1

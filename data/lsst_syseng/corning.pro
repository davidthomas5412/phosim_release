loadct,39
device,decomposed=0

for ii=0,1 do begin

;corning fused silica (2003) product sheet
;HPFS code 7980 standard grade
;22 degrees / 760 mm Hg
if ii eq 0 then begin
b1=double(0.68374049400)
b2=double(0.42032361300)
b3=double(0.58502748000)
c1=double(0.00460352869)
c2=double(0.01339688560)
c3=double(64.49327320000)
endif

;a0=double(2.104025406)
;a1=double(-1.456000330e-4)
;a2=double(-9.049135390e-3)
;a3=double(8.801830992e-3)
;a4=double(8.435237228e-5)
;a5=double(1.681656789e-6)
;a6=double(-1.675425449e-8)
;a7=double(8.326602461e-10)

;SHOTT BK7
;Malitson 1965, J Opt Soc Am 55, 1205-1208.
if ii eq 1 then begin
b1=double(0.6961663)
b2=double(0.4079426)
b3=double(0.8974794)
c1=double(0.0684043)^2
c2=double(0.1162414)^2
c3=double(9.896161)^2
endif

wavelength=0.3+dindgen(901)*(1.2-0.3)/900.

n=sqrt(1.0+b1*wavelength^2/(wavelength^2-c1)+$
  b2*wavelength^2/(wavelength^2-c2)+$
  b3*wavelength^2/(wavelength^2-c3))

;n=sqrt(a0+a1*wavelength^4+a2*wavelength^2+$
;       a3*wavelength^(-2)+a4*wavelength^(-4)+$
;       a5*wavelength^(-6)+a6*wavelength^(-8)+$
;       a7*wavelength^(-10))


if ii eq 0 then plot,wavelength,n,/ystyle,/xstyle
if ii eq 1 then oplot,wavelength,n,color=250,linestyle=2

;readcol,'silica_dispersion.txt',aa,bb
;oplot,aa,bb,linestyle=1,color=250


if ii eq 0 then openw,1,'silica_dispersion_corning.txt'
if ii eq 1 then openw,1,'silica_dispersion_malitson.txt'
for i=0L,N_elements(n)-1 do begin
   printf,1,wavelength(i),n(i)
endfor
close,1

endfor

END

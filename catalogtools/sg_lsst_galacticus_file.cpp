
///
/// @package phosim
/// @file SGGalaxyCatalogFile.cpp
/// @brief Class to read/write/access the records of the an entry of the LSST
///        Galacticus galaxy catalog
///
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author 
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include "sg_lsst_galacticus_file.h"


SGGalacticusLSSTFile::SGGalacticusLSSTFile()
{
  //Nothing here yet;
}
// **********************************************************************

SGGalacticusLSSTFile::~SGGalacticusLSSTFile()
{
  // Nothing here yet
}
// ***********************************************************************

void SGGalacticusLSSTFile::OpenGalacticusTextFile(std::string inputTxtFileName)
// *********************************************************************
// Opens for input the Text file version of the galaxy catalog
// First line(which we skip) is all the names of all the variables
// *********************************************************************
{
  if ( fGalacticusTextFile.is_open() ) {
    std::cout<<" SGGalacticusLSSTFile--Fatal--An input text file " 
	     << fGalacticusTextFileName << " is already open." << std::endl;
    exit(1);
  }

  // Open the input text file version of the catalog 
  fGalacticusTextFileName = inputTxtFileName;
  fGalacticusTextFile.open( fGalacticusTextFileName.c_str() );
  if( ! fGalacticusTextFile.is_open() ){
    std::cout<< " SGGalacticusLSSTFile--Fatal--Error Opening "
	     << fGalacticusTextFileName << std::endl;
    exit(1);
  }

  // *********************************************************************
  // Skip the first line
  // *********************************************************************
  std::string s;
  std::getline(fGalacticusTextFile, s);
  fRecordIndex=0;
  // *********************************************************************
  // See if this file has single SED filenames appended to it:
  // *********************************************************************
  std::string::size_type idx = s.find("DiskSEDFile");
  if (idx != std::string::npos ) {
    std::cout << "Input Galacticus file " << fGalacticusTextFileName 
	      << " has SED file names appended."<<std::endl;
    fHasSED = true;
  }

  // ******************************
  // And we are ready to read it in line by line
  // ******************************
  return;
}
// ************************************************************************
 
bool SGGalacticusLSSTFile::ReadGalacticusTextFile(
					   std::vector< double >& dataRecord,
					   std::string& BulgeSEDFileName,
					   std::string& DiskSEDFileName)
// ************************************************************************* 
// Read a record from the Galacticus input text file and place into the 
// dataRecord vecotr.
// We can use the SGGalacticusElements enum to find particular elements in the
// data vector.
// *************************************************************************
{
  // ***********************************************************************
  // There are mixed double's and int's in this record but we will read them 
  // all as doubles
  // ***********************************************************************
  while(1) {
    std::string galaxy;
    std::getline(fGalacticusTextFile, galaxy);  //Get a galaxy line
    if(fGalacticusTextFile.eof()) {
      std::cout<<" SGLSSTGalacticus--EOF found after " << fRecordIndex 
			   << "records."<< std::endl;
      return false;
    }
	// **************************************************
	// Check the goodness of this galaxy entry line
	// If this entry  has "inf" and "nan" values in it, We need to skip those
	// lines if any of the values in the line that we use have those values.
	// **************************************************
	bool goodLine =IsEntryLineGood(galaxy,fRecordIndex); 

	if (goodLine) {
	  // **********************************************************************
	  // Now parse the record into the data vector
	  // **********************************************************************
	  std::string value;
	  BulgeSEDFileName=" ";
	  DiskSEDFileName=" ";
 	  dataRecord.clear();
	  std::istringstream galaxySS(galaxy);
	  while (galaxySS>>value) {
		if (dataRecord.size() != GE_BULGE_SED_FILE_NAME  ) {
		  if (value == "inf" || value == "nan" || value == "-inf" ) {
			dataRecord.push_back(0.0);
		  }
		  else{
			double dataValue;
			std::istringstream isVal(value);
			isVal >> dataValue;
			dataRecord.push_back(dataValue);
		  } 
		}
		else {	  //We have the sed file name
		  BulgeSEDFileName=value;
		  galaxySS>>value;
		  DiskSEDFileName=value;
		  break;
		}
	  }
	}
	else {
      continue;   //Skip this line and Do next line
    }

    fRecordIndex++;   //count the galaxies read

    // ******************************************************
    // Done, check some things
    // *****************************************************
    if ((int) dataRecord.size() != GE_BULGE_LSST_Y4_OBSERVED+1) {
      std::cout<<" SGLSSTGalacticus--Fatal--Wrong length line in input file"
	       <<std::endl;
      std::cout<< " in Galacticus text file at line "<<fRecordIndex
	       <<std::endl;;
      std::cout<<"expected: "<< GE_BULGE_LSST_Y4_OBSERVED+1 << " got: "
	       << dataRecord.size() << std::endl;
      exit(1);
    }
    if( fGalacticusTextFile.eof()) {
      return false;
    }
    else{
      return true; //Not the best error checking but should work for things 
    }              //fairly well
  }
}
// ***********************************************************************

void  SGGalacticusLSSTFile::CloseGalacticusTextFile()
{
  if (fGalacticusTextFile == NULL) {
    std::cout<<"SGLSSTGalacticus--No Galacticus Text File opend to close"
	     <<std::endl;
    exit(1);
  }
  fGalacticusTextFile.close();
  return;
}
// **********************************************************************

bool SGGalacticusLSSTFile::IsEntryLineGood(std::string galaxyEntry,
													int recordIndex)
// *********************************************************************
// Check that the goodness of the galaxy entry in the Galacticus file.
// If wrong nuymber of values in line, exit.
// Or if any of the entries in the line that we use are nan or inf, 
// return false
// *********************************************************************
{
  std::istringstream galaxySS(galaxyEntry);
  int entryCount = 0;
  std::string value;
  while (galaxySS>>value) {
	entryCount++;
	if ( entryCount > GE_DISK_SED_FILE_NAME+1) {
	  std::cout << "Too many values ( "<<entryCount<<" on input line " 
				<< fRecordIndex+1 
				<< " of file " << fGalacticusTextFileName << std::endl;
	  std::cout<<"Should have "<<GE_DISK_SED_FILE_NAME+1<<std::endl;
	  std::cout<<"Excess value: "<<value<<std::endl;
	  exit(1);
	}
	
	if (value == "inf" || value == "nan" || value == "-inf" ) {
	  if( std::find(checkElementList.begin(), checkElementList.end(), 
				   entryCount-1) == checkElementList.end() ) {
		bool goodLine=false;
		return goodLine;
	  }
	}
  }
  return true;
}
// *********************************************************************

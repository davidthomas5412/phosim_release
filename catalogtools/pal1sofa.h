/*
*+
*  Name:
*     pal1sofa.h

*  Purpose:
*     Mappings of ERFA names to SOFA names

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Include file

*  Invocation:
*     #include "pal1.h"

*  Description:
*     PAL will work with both SOFA and ERFA libraries and the
*     difference is generally a change in prefix. This include
*     file maps the ERFA form of functions to the SOFA form
*     and includes the relevant sofa.h vs erfa.h file.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - PAL uses the ERFA form by default.

*  History:
*     2014-07-29 (TIMJ):
*        Initial version
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2014 Tim Jenness
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#ifndef PAL1SOFAHDEF
#define PAL1SOFAHDEF


#  include "erfa.h"
#  include "erfam.h"

/* No further action required */

# endif



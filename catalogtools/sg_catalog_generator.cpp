///
/// @package phosim
/// @file stars_and_galaxies_generator.cpp
/// @brief Generates phosim Object catalog and instance file with 
///        random or grid of stars and/or galaxies (galaxies not 
///        implimented yet)
///
/// @brief Created by:/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author 
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <iomanip>
#include <algorithm>    // std::min_element, std::max_element

#include "pal.h"
#include "palmac.h"
#include "erfa.h"
#include "erfam.h"


#include "azelradecxy.h"

#include "readtext.h"
using readtext::readText;

#include "basic_types.h"
#include "rng_mwc.h"
using namespace RandomNumbers;

#include "sg_lsst_galacticus_file.h"
#include "sg_lsst_buzzard_file.h"
#include "sunmoon.h"

const double kBulgeToTotalFlux      = .5541;
const double kBulgeToTotalFluxSigma = .0198;
const double kMJD                   = 49552.3;
const int    kListNumberCount       = 10000;

// Note: We use PAL__DPI from palm.h for pi!
// *****************************************************************

class StarsAndGalaxiesParameters
{
public:
  StarsAndGalaxiesParameters(int argc, char* argv[]);
  void SetDefaults();
  void SetRandomTelescopeDefaults();
  void SetRandomAltAz();
  void ReadParameters(readText& pars);
  std::string GetInputFileName(){return  fInputParFileName;};

  void Help();
  void Usage();

  bool IsHelpRequest(){return fHelp;};
  bool IsUsageRequest(){return fUsage;};
  bool IsRandomStars(){return fRandomStars;};
  bool IsStarGrid(){return fStarGrid;};
  bool IsCustomStars(){return fCustomStars;};
  bool IsRandomGalaxies(){return fRandomGalaxies;};
  bool IsGalaxyGrid(){return fGalaxyGrid;};
  
private:
  bool fHelp;
  bool fUsage;
  bool fRandomStars;
  bool fStarGrid;
  bool fCustomStars;

  bool fRandomGalaxies;
  bool fGalaxyGrid;

public:
  // *******************************Instance parameters
  int         rndmStarsNumber;
  double      rndmStarsFOVDiameterDeg;
  double      rndmStarsLimitingMagnitude;
  std::string rndmStarsSEDFileName;

  double      starGridWidthDeg;
  double      starGridSpacingDeg;
  double      starGridFixedMagnitude;
  std::string starGridSEDFileName;

  std::string starXYMFile;

  std::string GalacticusLSSTFileName;
  std::string BuzzardTruthFileName;

  int         rndmGalaxiesNumber;
  double      rndmGalaxiesFOVDiameterDeg;
  double      rndmGalaxyLimitingMagnitude;

  double      galaxyGridWidthDeg;
  double      galaxyGridSpacingDeg;
  double      galaxyGridMagnitude;

  std::string fObjectCatalogFileName;
  std::string fInstanceFileName;

  double      mjd;
  std::string moonOption;
  double      moondec;
  double      moonra;
  double      moonalt;
  double      dist2moon;
  double      moonphase;

  std::string sunOption;
  double      sunalt;


  // "Observer" Controls:
  double      trkAzimuthDeg;
  double      trkAltitudeDeg;
  double      unrefractedRADeg;
  double      unrefractedDecDeg;
  int         filter;
  double      cameraRotationDeg;
  double      exposureTimeSec;
  int         numSnaps;

  // *******************************************end Paramters

  double latitudeDeg;
  double eastLongitudeDeg;
  uint32 ranseed;
  int    catalogObjectID;
  std::string fInputParFileName;
  bool fDebugPrint;
};
// *******************************************************

StarsAndGalaxiesParameters::StarsAndGalaxiesParameters(int argc, 
						       char* argv[])
{
  fDebugPrint=false;

  // **************************************************************
  // If no input arguments exist, treat as a -h  help request
  if ( argc == 1 ) {
	fHelp  = true;
    return;
  }

  // **************************************************************
  // Otherwise the first argument must be one of the folowing:
  // -h  or -u  or a .pars command file name.
  // Note we DO NOT allow the use of a "=" in any of the commands
  // **************************************************************
  string optArg = argv[1];
  fUsage = false;
  //  std::cout << "optArg: " << optArg << std::endl;
  if ( optArg == "-h") {
	fHelp = true;
	return;
  }
  else {
	if ( optArg == "-u") {
	  fUsage = true;
	  return;
	}
  }
 
  // *******************************************************************
  // since -h and -u are only allowed "options" look for  a command line 
  // argument that is the name of the .pars file  to use with this program
  // *******************************************************************
  // Check that this command line argument is not a mistaken attempt to
  // specify an option (make sure first charagter is not a "-")
  // ********************************************************************
  if (optArg.at(0) == '-' ) {
	std::cout<<"Error--Illegal option on command line:" << optArg 
			 << std::endl;
	fHelp = true;
	return;
  }

  // *******************************************************************
  // Next the .pars file name should be the only argument
  // *******************************************************************
  if ( argc != 2 ) {

	std::cout << " Fatal -- Too many command line arguments!" << std:: endl;
	fHelp  = true;
    return;
  }

  // **********************************************************
  // We have a command line argument that should be the name of the .pars file 
  // to use with this program.  Pickup the input parameter file name
  // ************************************************************
  fInputParFileName=optArg;
  return;
}
// ***********************************************************


void StarsAndGalaxiesParameters::SetDefaults()
{
  rndmStarsNumber            = 10000;
  rndmStarsFOVDiameterDeg    = 6.0;
  rndmStarsLimitingMagnitude = 28;
  rndmStarsSEDFileName       = "../sky/sed_flat.txt";


  starGridWidthDeg       = 6.0;
  starGridSpacingDeg     = 0.01;
  starGridFixedMagnitude = 20.0;
  starGridSEDFileName    = "../sky/sed_flat.txt";

  starXYMFile = " ";
  
  rndmGalaxiesNumber          = 10000;
  rndmGalaxyLimitingMagnitude = 28;

  galaxyGridWidthDeg          = 6.0;
  galaxyGridSpacingDeg        = 0.01;
  galaxyGridMagnitude         = 20.;

  GalacticusLSSTFileName      = " ";
  BuzzardTruthFileName        = " ";


  fObjectCatalogFileName = "SGCatalog.cat";
  fInstanceFileName      = "SGInstance.inst";
  latitudeDeg           = -30.66; //LSST location
  eastLongitudeDeg      = -70.7494; //eastLong is positive to east
  catalogObjectID       = 0;

  moonOption = "BelowHorizen";
  moondec    = -90.0;
  moonra     = 180.0;
  moonalt    = -90.0;
  dist2moon  = 180.0;
  moonphase  = 10.0;

  sunOption  = "Off";
  sunalt     = -90.0;

  //Observer controls
  trkAzimuthDeg     = -1.0;
  trkAltitudeDeg    = -1.0;
  unrefractedRADeg  = -1.0;
  unrefractedDecDeg = -1.0;
  exposureTimeSec   = 15.0;
  numSnaps          = 1;

  ranseed    = 1000;

  return;
}
// ********************************************************

void StarsAndGalaxiesParameters::SetRandomAltAz()
// ***********************************************************
// Set random values for Alt Az
// ***********************************************************
{
  trkAzimuthDeg =  RngDouble()* 360.0;
  if ( trkAzimuthDeg >= 360.0) {
	trkAzimuthDeg = 0.0;
  }
  
  // ********************************************************
  // use method from http://mathworld.wolfram.com/SpherePointPicking.html
  // for elevation selection? Select between 45-90 deg.
  // ********************************************************
  double r =  RngDouble();
  double lowR=(cos(PAL__DPI/4.)+1.)/2.; 
  double ranInRange = r * (1- lowR) + lowR;
  
  double trkZenith = acos(2*ranInRange-1.0) * 180 / PAL__DPI;
  
  trkAltitudeDeg= 90.0-trkZenith;

  if ( trkAltitudeDeg >= 90.0) {
	trkAltitudeDeg = 89.9999;
  }
  std::cout << "Using Random Alt/Az:" <<std::endl;
  std::cout << "    trkAltitudeDeg:    " << trkAltitudeDeg <<std::endl;
  std::cout << "    trkAzimuthDeg:     " << trkAzimuthDeg <<std::endl;
  return;
}
// ******************************************************************


void StarsAndGalaxiesParameters::SetRandomTelescopeDefaults()
// ****************************************************************
// The RandomObservationMode command has been found. Set defulats of the 5
// Observer/Telescope Operator to random values over obvious ranges
// The telescope Operator commands can than be used to override these values.
// ****************************************************************
{
  filter = RngDouble()*6;//using round down here
  if(filter == 6) {
	filter = 0;
  }
  
  cameraRotationDeg =RngDouble()*360; 
  if (cameraRotationDeg >= 360.0) {
	cameraRotationDeg = 0.0;
  }

  mjd = kMJD + RngDouble()*10*365.25;  //Ten year spread

  // **********************************************************
  // We leave exposureTimeSec at its 15 sec default value.
  // We leave numSnaps at 1;
  // We let Ra/Dec be set from Alt/AZ and MJD
  // **********************************************************
	std::cout << "Some telescope operator options not explictly set. Using random "
                 "values:" <<std::endl;
	std::cout << "    filter:            " << filter <<std::endl;
	std::cout << std::fixed  << std::setprecision(4)
			  << "    cameraRotationDeg: " << cameraRotationDeg <<std::endl;
	std::cout << std::fixed  << std::setprecision(8)      
			  << "    mjd:               " << mjd <<std::endl;

  return;
}
// **********************************************************************

void StarsAndGalaxiesParameters::Help()
{
  SetDefaults();

  std::cout << "******************sg_catalog_generator **********************"
			<< std::endl << std::endl;
  std::cout << " ./sg_catalog_generator [OPTION]  [COMMANDFILE] "<< std::endl
			<< std::endl;
  std::cout << "     Generate PhoSim compatible Instance and Catalog "
	           "files." << std::endl << std::endl;
  std::cout << "**Only one of the following options is allowed on the command line:" 
			<< std::endl 
			<< std::endl;
  std::cout << "     -h                  Prints this help message" << std::endl;
  std::cout << "     -u                  Prints full detailed usage message "
               "(see below)" << std::endl << std::endl;
  std::cout << "**Or a single command line argument:" << std::endl 
			<< std::endl;
  std::cout << "     COMMANDFILE     Execute catalog generation commands "
               "from COMMANDFILE" << std::endl << std::endl;
  std::cout << "**Where the catalog generation commands in the COMMANDFILE can "
	"be are any of "<< std::endl;
  std::cout << "  the following:" << std::endl << std::endl;
  std::cout << "         RandomStars             For a catalog of Random Stars"
			<< std::endl << std::endl;
  std::cout << "         StarGrid                A catalog of a Grid of Stars"
			<< std::endl << std::endl;
  std::cout << "         CustomStars  STARFILE   A catalog of Custom Stars "
               "from a file" << std::endl << std::endl;


  std::cout << "  Telescope Operator Commands (All Operator commands "
	           "can be defaulted)" << std::endl << std::endl;
  std::cout << "         Unrefracted_Altitude Alt  Telescope Pointing: "
               "Unrefracted Altitude(deg)" << std::endl;
  std::cout << "                                   Default: Random(45-90 deg)"
			<< std::endl;
  std::cout << "         Unrefracted_Azimuth Az    Telescope Pointing: "
	"Unrefracted Azimuth(deg)" << std::endl;
  std::cout << "                                   Default: Random(0-360) deg"
			<< std::endl;
  std::cout << "         Filter N                  Filter number. "
			<< std::endl;  
  std::cout << "                                   Default: Random(0-5)" 
			<< std::endl;  
  std::cout << "         CameraRotation Angle      Angle(deg) camera "
               "rotated"<< std::endl;
  std::cout << "                                   Default: Random(0-360)"
			<< std::endl;
  std::cout << "         ExposureTime Time         Time(sec) of the "
               "exposure."<< std::endl; 
  std::cout << "                                   Default: " 
			<< std::fixed  << std::setprecision(1)
			<< exposureTimeSec << std::endl;
  std::cout << "         NumberOfExposures N       Number of SNAPshots to take"
			<< std::endl;
  std::cout << "                                   Default: " 
			<< numSnaps << std::endl;
  std::cout << "         MJD  Date                 Date (in MJD) of "
	           "observation." << std::endl;
  std::cout << "                                   Default: Random(" 
			<< std::fixed << std::setprecision(1)  
			<< kMJD <<" - "<< kMJD+10*365 << ") "<< std::endl << std::endl;
  std::cout << "    Detailed descriptions of all these options and all "
               "additional supporting "<< std::endl;
  std::cout << "  and customizing ancillary options can be found by "
               "use of the "<< std::endl;
  std::cout << "  '-u' (usage) command option." << std::endl;
  std::cout << "    The acceptable range of all command values and their "
               "defaults are also"<< std::endl;
  std::cout << "  shown within the '-u' (usage) listing." << std::endl;
  return;
}
// ***********************************************************************
 

void StarsAndGalaxiesParameters::Usage()
{
  SetDefaults();
  std::cout << std::endl; 
  std::cout << "*********Details of the COMMANDFILE Catalog Commands********" 
			<< std::endl; 
  std::cout << "**For Command 'RandomStars':" << std::endl << std::endl;
  std::cout << "           NumberOfRandomStars N        Where N is number of "
               "stars to generate." << std::endl; 
  std::cout << "                                        Default: " 
			<< rndmStarsNumber << std::endl;
  std::cout << "           RandomStarFieldOfView D      Where D is Diameter"
               "(deg) of the" << std::endl;
  std::cout << "                                        Field-of-View. " 
			<< std::endl;
  std::cout << "                                        Default: " 
			<< rndmStarsFOVDiameterDeg	<< std::endl;
  std::cout << "           RandomStarMagnitudeLimit M   Limiting magnitude."
			<< std::endl;
  std::cout << "                                        Default: " 
			<< rndmStarsLimitingMagnitude << std::endl << std::endl; 

  //????Add random star SED name or method of generation of name

  std::cout << "**For command 'StarGrid':" << std::endl << std::endl;
  std::cout << "           StarGridWidthDeg W         Where W is grid  Width "
               "and Height(deg) " << std::endl;
  std::cout << "                                      Default: " 
			<< starGridWidthDeg	<< std::endl;
  std::cout << "           StarGridMagnitude M        Magnitude of all stars "
               "in grid." << std::endl;
  std::cout << "                                      Default: " 
			<< starGridFixedMagnitude << std::endl;
  std::cout << "           StarGridSpacingDeg S       Spacing(deg) of stars "
	           "in grid."<< std::endl; 
  std::cout << "                                      Default: " 
			<< std::fixed  << std::setprecision(4)
			<< starGridSpacingDeg << std::endl;
  std::cout << "           StarGridSEDFileName Name   Grid Stars SED file name."
			<< std::endl; 
  std::cout << "                                      Default: '"
	        << starGridSEDFileName << "'" << std::endl << std::endl;

  // ???? Add sed name to file arguments
  std::cout << "**For command 'CustomStars   STARFILE : " << std::endl 
			<< std::endl;
  std::cout << "           STARFILE is the name of the custom star "
               "file name."<< std::endl;
  std::cout << "           Custom star file format: X(deg) Y(deg) "
               "Magnitude SEDFile" << std::endl << std::endl;

  // ********************************************************************
  //Not implimented yet
  // std::cout << " *** If you wantRandom Galaxies:" << std::endl;
  // std::cout << "   RandomNumberOfGalaxies N       : N = Number of random "
  //              "galaxies to add to catalog (default: 0).Other params "
  //             "can default:" " << std::endl;
  // std::cout << "                 RandomGalaxyFieldOfView D : D = Diameter 
  //                "in degrees of random star field-of-view (default:  "
  //              "6.0)" << std::endl;
  // std::cout << "                 RandomGalaxyMagnitudeLimit M: M = "
  //              "Limiting mag of random galaxies to add to catalog. "
  //              "(default/max:  28)"  
  // ********************************************************************
  //std::cout << "**For Galacticus Galaxies: GalacticusLSSTFile Name: Name => "
  //           "Galacticus file." << std::endl;
  //std::cout << "     (Not specified =>  no galaxies)"  << std::endl;
  //
  //std::cout << "**For Buzzard-highres Galaxies:(not yet implimented!!! " 
  //		<< std::endl; 
  //std::cout << "   BuzzardTruthFitsFile Name: Where Name => Buzzard-highres "
  //           "HEALPix Fits file." << std::endl;
  //std::cout << "      (Not specified => no galaxies)"<< std::endl;
 
  std::cout << "********Output files: Phosim File Name Commands************* " 
			<< std::endl;
  std::cout << "  This program will APPEND to any existing Catalog file that "
               "is specified. " <<std::endl;
  std::cout << "  A new Catalog file will be created if needed." 
			<< std::endl;
  std::cout << "  A new Instance file will alwyas be created (replacing any "
	           "existing file "<< std::endl;
  std::cout << "  of the same name)."<< std::endl << std::endl;
  std::cout << "           InstanceFile Name        Output Phosim Instance file"
               "name" << std::endl;
  std::cout << "                                    Default: '"
			<< fInstanceFileName <<"'" << std::endl;
  std::cout << "           ObjectCatalogFile Name   Output Phosim Catalog file"
               "name" << std::endl;
  std::cout << "                                    Default: '"
			<< fObjectCatalogFileName << "'"	<< std::endl << std::endl;

  // Telescope operator controls
  std::cout << "******************Telescope Operator Commands*************** " 
			<< std::endl;
  std::cout << "  Some of the Telescope Operator Commands are defaulted to "
               "random values. If" << std::endl;
  std::cout << "  the user wants a specific value for an operator command it "
	"must be specified " <<std::endl; 
  std::cout << "  explitly in the COMMANDFILE." << std::endl << std::endl;
  std::cout << "           Unrefracted_Altitude Alt  Where Alt is Unrefracted "
               "Altitude(deg)"<< std::endl; 
  std::cout << "                                     (see note below)"	
			<< std::endl; 
  std::cout << "                                     Default: Random(45-90 deg)"
			<< std::endl;
  std::cout << "           Unrefracted_Azimuth Az    Where Az is Unrefracted "
               "Azimuth (deg)"<< std::endl;
  std::cout << "                                     (see note below)"	
			<< std::endl;
  std::cout << "                                     Default: Random(0-360) deg"
			<< std::endl;
  std::cout << "           Filter N                  Where N is Filter number."
			<< std::endl;
  std::cout << "                                     Default: Random(0-5)" 
			<< std::endl;  
  std::cout << "           CameraRotation Angle      Where Angle is camera "
               "rotation angle(deg)"<< std::endl; 
  std::cout << "                                     Default: Random(0-360)"
			<< std::endl;

  std::cout << "           ExposureTime Time         Where Time(sec) is "
               "length of the exposure."<< std::endl; 
  std::cout << "                                     Default: " 
			<< std::fixed  << std::setprecision(1)
			<< exposureTimeSec << std::endl;
  std::cout << "           NumberOfExposures N       Number of SNAPshots to "
               "take" << std::endl;
  std::cout << "                                     Default: " 
			<< numSnaps << std::endl;
  std::cout << "           MJD  Date                 Date (in MJD) of "
	           "observation." << std::endl;
  std::cout << "                                     Default: Random(" 
			<< std::fixed << std::setprecision(1)  
			<< kMJD <<" - "<< kMJD+10*365 << ")" << std::endl << std::endl;
  std::cout << "  Note that the RA/Dec pair of pointing values, if not "
               "explicitly specfied," << std::endl;
  std::cout << "  are derived from the MJD and Alt/Az values(all 3 of which "
	           "can be defaulted to " << std::endl;
  std::cout << "  have random values).  The Telescope commands and the MJD "
               "command can "<< std::endl;
  std::cout << "  individually be used to override these values. If RA/Dec "
	           "pair of pointing " << std::endl;  
  std::cout << "  is specified, but not the Alt/Az, then the Alt/Az will be "
               "generated using the "<< std::endl;
  std::cout << "  specified RA/Dec and the MJD. Specifying both Ra/Dec and "
               "Alt/Az values " << std::endl;
  std::cout << "  will cause those values to be used as given (even if they "
               "are inconsistent " << std::endl;
  std::cout << "  for the given MJD!):"<< std::endl << std::endl;
  std::cout << "           Unrefracted_RA_deg RA     Where RA is the "
               "Unrefracted  Right "<< std::endl;
  std::cout << "                                     Ascension (deg)" 
			<< std::endl;
  std::cout << "                                     Default: See Above" 
			<< std::endl;
  std::cout << "           Unrefracted_Dec_deg Dec   Where Dec is the "
	           "Unrefracted " << std::endl;
  std::cout << "                                     Declination (deg)" 
			<< std::endl;
  std::cout << "                                     Default: See Above" 
			<< std::endl << std::endl;

 

  std::cout << "******************Miscellaneous*****************************" 
			<<std::endl <<std::endl;
  std::cout << "           Moon   Option        Where Option is one of "
               "'BelowHorizen' " <<std::endl;
  std::cout << "                                or 'Realistic'(ie.Location/"
               "phase from MJD)" <<std::endl;
  std::cout << "                                Default: '" << moonOption 
			<< "'" << std::endl;
  std::cout << "           Sun    Option        Where Option is one of "
               "'Off' or 'Realistic' "<<std::endl;
  std::cout << "                                (Realistic is location from "
               "MJD)"<<std::endl;
  std::cout << "                                Default: '" << sunOption 
			<< "'" << std::endl;
  std::cout << "           RandomSeed Seed      Where Seed is the Random "
               "number generator " << std::endl;
  std::cout << "                                seed(int) " << std::endl;
  std::cout << "                                Default: " << ranseed 
			<< std::endl;
  std::cout << "************************************************************" 
			<< std::endl;
  return;
}
// *****************************************************

void StarsAndGalaxiesParameters::ReadParameters(readText& pars)
{
  // *****************************************************
  // get input params
  // *****************************************************
  int  parsNumArgs = pars.getSize();  //Number of lines in the command file

  // ******************************************************************
  // Get a new random seed if it was set and use it for this program also
  // ******************************************************************
  for (unsigned long t=0; t < (unsigned long) parsNumArgs; t++) {
    std::string line = (std::string)pars[t]; 
    bool gotKey=false;
	gotKey = readText::getKey(line, "RandomSeed", ranseed);
	if ( gotKey ) {
	  break;
	}
  }

  RngSetSeed32(ranseed);  // Need to do this before we use random numbers

  // ******************************************************************
  // Set up all the command default values
  // ******************************************************************
  SetDefaults();
  SetRandomTelescopeDefaults();

  // *******************************************************************
  // Process the commands
  // *******************************************************************
  fRandomStars = false;
  fStarGrid    = false;
  fCustomStars = false;

  
  // ********************************************************
  // Reset to start of file. Found commands will override defaults
  // ********************************************************
  
  for (unsigned long t=0; t < (unsigned long) parsNumArgs; t++) {
	//Get the next line from the input file.
	std::string line = (std::string)pars[t]; 
	
	// *****************************************************
	// Look to see what type of catalog we are making
	// This also means we find all commands with no arguments, thus the
	// 'continue'. getKey method would fail on such commands I think.
	// *****************************************************
	bool foundKey = readText::findKey(line,"RandomStars");
	if(foundKey) {
	  fRandomStars = true;
	  continue;
	}
	foundKey = readText::findKey(line,"StarGrid");
	if(foundKey) {
	  fStarGrid = true;
	  continue;
	}
	foundKey = readText::findKey(line,"CustomStars");
	if(foundKey) {
	  fCustomStars = true;
	  // **********************************************************
	  // Make sure ther is a second argument and get it 
	  // (the custom star file name)
	  // Note no error checking here
	  // **********************************************************
	  foundKey = readText::getKey(line,"CustomStars", starXYMFile); 
	  continue;
	}

	// ******************************************************
	// Check for various commands whose values we need.
    // ******************************************************
    bool gotKey=false;
    gotKey = gotKey || readText::getKey(line, "NumberOfRandomStars", 
										rndmStarsNumber);
    gotKey = gotKey || readText::getKey(line, "RandomStarFieldOfView",         
										rndmStarsFOVDiameterDeg);
    gotKey = gotKey || readText::getKey(line, "RandomStarMagnitudeLimit",
					rndmStarsLimitingMagnitude);
 
	gotKey = gotKey || readText::getKey(line, "StarGridWidthDeg",  
					starGridWidthDeg);
    gotKey = gotKey || readText::getKey(line, "StarMagnitude", 
					starGridFixedMagnitude);
    gotKey = gotKey || readText::getKey(line, "StarGridSpacingDeg", 
					starGridSpacingDeg);
    gotKey = gotKey || readText::getKey(line, "StarGridSEDFileName", 
					starGridSEDFileName);

    gotKey = gotKey || readText::getKey(line, "StarsInFOVFile", 
					starXYMFile);

    gotKey = gotKey || readText::getKey(line, "GalacticusLSSTFile",
					GalacticusLSSTFileName);
    gotKey = gotKey || readText::getKey(line, "BuzzardTruthFitsFile",
					BuzzardTruthFileName);

    gotKey = gotKey || readText::getKey(line, "InstanceFile", 
					fInstanceFileName);
    gotKey = gotKey || readText::getKey(line, "ObjectCatalogFile", 
					fObjectCatalogFileName);

    // folowing are defined but not implimented yet
	gotKey = gotKey || readText::getKey(line, "RandomNumberOfGalaxies", 
					rndmGalaxiesNumber);
    gotKey = gotKey || readText::getKey(line, "RandomGalaxyMagnitudeLimit",
					rndmGalaxyLimitingMagnitude);
    
	gotKey = gotKey || readText::getKey(line, "GalaxyGridWidthDeg",  
					galaxyGridWidthDeg);
    gotKey = gotKey || readText::getKey(line, "GalaxyGridSpacingDeg", 
					galaxyGridSpacingDeg);
    gotKey = gotKey || readText::getKey(line, "GridGalaxyMagnitude", 
					galaxyGridMagnitude);
    
    // *************************************************************
    // For the names of some of the parameters,  match the instance 
    // catalog names.
    // **************************************************************
    gotKey = gotKey || readText::getKey(line, "SedFileName", 
										starGridSEDFileName);

    gotKey = gotKey || readText::getKey(line, "Unrefracted_Azimuth", 
										trkAzimuthDeg);

    gotKey = gotKey || readText::getKey(line, "Unrefracted_Altitude",
										trkAltitudeDeg);

    gotKey = gotKey || readText::getKey(line, "Unrefracted_RA_deg", 
										unrefractedRADeg);

    gotKey = gotKey || readText::getKey(line, "Unrefracted_Dec_deg",
										unrefractedDecDeg);
	
    gotKey = gotKey || readText::getKey(line, "Filter", filter);

    gotKey = gotKey || readText::getKey(line, "CameraRotation", 
										cameraRotationDeg);

    gotKey = gotKey || readText::getKey(line, "ExposureTime", 
										exposureTimeSec);
    gotKey = gotKey || readText::getKey(line, "NumberOfExposures", 
										numSnaps);


    gotKey = gotKey || readText::getKey(line, "Moon", moonOption);
    gotKey = gotKey || readText::getKey(line, "Sun",  sunOption);

    gotKey = gotKey || readText::getKey(line, "MJD", mjd);

	gotKey = gotKey || readText::getKey(line, "RandomSeed", ranseed);

    if ( ! gotKey ) {
      std::cout<<" Unknown Option: " << line << std::endl;
      exit(1);
    }
  }  

  // **********************************************************************
  // Make sure we have at least on valid catalog request command
  // ********************************************************************** 
  if ( ! (fRandomStars || fStarGrid || fCustomStars) ) {
    std::cout << "Fatal- One or more of the following commands must be in the "
	  "command file: " << std::endl;
	std::cout << "          RandomStars" <<std::endl;
	std::cout << "          StarGrid" <<std::endl;
	std::cout << "          CustomStars" <<std::endl;
	std::cout << "Fatal- No valid catalog request" <<std::endl;

	Help();
	exit(1);
  }

  // ********************************************************
  // Set some limits
  // ********************************************************
  if ( rndmStarsLimitingMagnitude>28 ) {
    rndmStarsLimitingMagnitude=28;
  }
  if ( rndmGalaxyLimitingMagnitude>28 ) {
    rndmGalaxyLimitingMagnitude=28;
  }

  return;
}  
// *****************************************************************

class StarsAndGalaxiesInstanceFile
{
public:
  StarsAndGalaxiesInstanceFile(){};
  AzElRADecXY* pSGInstAzelradec;
  void CreateInstanceFile( std::ofstream& instanceFile, 
			   StarsAndGalaxiesParameters* pSGParams );

};
// ******************************************************************

void StarsAndGalaxiesInstanceFile::CreateInstanceFile(
			      std::ofstream& instanceFile, 
			      StarsAndGalaxiesParameters*  pSGParams )
{
  // ****************************************************************
  // Make up an instance file simular to that of example/star for 
  // the Stars and Galaxies Object catalog we are creating.
  // ****************************************************************
  // If either an Alt/Az pair or a RA/Dec pair of pointing values
  // can be specified. Whichever pair is not specified will 
  // be generated using MJD. 
  // Specifying both Ra/Dec and Alt/Az values will case those 
  // values to be used as given (even if they are inconsistent!)
  // Specifying neither will cause a random alt/az to be generated and the 
  // RaDec will be generated from that and the MJD.
  // This is all done using the pallib (and thus the sofalib) functions
  // ****************************************************************
  // Default to lsst location for now.
  // ****************************************************************
  pSGInstAzelradec = new AzElRADecXY(  
			   pSGParams->eastLongitudeDeg * PAL__DPI / 180.0,  
			   pSGParams->latitudeDeg * PAL__DPI / 180.0); 
  
  // *****************************************************************
  // If all of az,alt,ra,dec not specified in input generate random AltAz
  // (Note this sets AltAz for next test below)
  // If not set, SetDefaults will have set to -1.0
  // *****************************************************************
  if( pSGParams->trkAzimuthDeg     < -0.5 && 
	  pSGParams->trkAzimuthDeg     < -0.5 &&
	  pSGParams->unrefractedRADeg  < -0.5 && 
	  pSGParams->unrefractedDecDeg < -0.5 ) {
	
	pSGParams->SetRandomAltAz();
  }

  // *****************************************************************
  // If Alz/AZ specified and RA/Dec is not, Generate Ra/Dec from Alt,Az,MJD
  // AltAz may have either been set by user in commandfile or randomly by this
  // program (above)
  // *****************************************************************
  if(pSGParams->trkAzimuthDeg     > -0.5 && 
     pSGParams->trkAzimuthDeg     > -0.5 &&  
     pSGParams->unrefractedRADeg  < -0.5 && 
     pSGParams->unrefractedDecDeg < -0.5 ) {
    double raRad;
    double decRad;
    pSGInstAzelradec->AzEl2RADec2000( 
				     pSGParams->trkAzimuthDeg * PAL__DPI / 180.0, 
				     pSGParams->trkAltitudeDeg * PAL__DPI / 180.0, 
				     pSGParams->mjd, raRad, decRad);
    pSGParams->unrefractedRADeg  = raRad  * 180.0 / PAL__DPI;
    pSGParams->unrefractedDecDeg = decRad * 180.0 / PAL__DPI;
  }
  else {
    // *************************************************************
    // If Ra/Dec specified and AltAz not, generate  Atl/Az from Ra/Dec,MJD
    // ***************************************************************
    if(pSGParams->trkAzimuthDeg     < -0.5 && 
       pSGParams->trkAzimuthDeg     < -0.5 &&  
       pSGParams->unrefractedRADeg  > -0.5 && 
       pSGParams->unrefractedDecDeg > -0.5 ) {
      double azRad;
      double elevRad;
      pSGInstAzelradec->RADec2000ToAzEl( 
					pSGParams->unrefractedRADeg,
					pSGParams->unrefractedDecDeg,
					pSGParams->mjd, azRad, elevRad);
      
      pSGParams->trkAzimuthDeg  = azRad * 180.0 / PAL__DPI;
      pSGParams->trkAltitudeDeg = elevRad * 180.0 / PAL__DPI;
    }
  }
  // ***************************************************************
  // Add Pointing specs and time to instance file
  // ***************************************************************
  instanceFile << std::fixed << std::setprecision(8)    
			   << "Unrefracted_RA_deg "   << pSGParams->unrefractedRADeg  
			   << std::endl;
  instanceFile << "Unrefracted_Dec_deg "  << pSGParams->unrefractedDecDeg 
			   << std::endl;
  instanceFile << "Unrefracted_Azimuth "  << pSGParams->trkAzimuthDeg
			   << std::endl;
  instanceFile << "Unrefracted_Altitude " << pSGParams->trkAltitudeDeg 
			   << std::endl;
  instanceFile << "Opsim_expmjd "         << pSGParams->mjd            
			   << std::endl;

  // *****************************************************************
  // Add the remianing "Telescope Operator" settings
  // Note Opsim_rotskypos this get turned into 'rotationangle' by phosim.py
  // *****************************************************************
  instanceFile << "Opsim_filter " << pSGParams->filter << std::endl;
  instanceFile << std::fixed << std::setprecision(2)    
			   << "SIM_VISTIME  " << pSGParams->exposureTimeSec <<  std::endl;
  instanceFile << "SIM_NSNAP "    << pSGParams->numSnaps << std::endl;
  instanceFile << std::fixed << std::setprecision(4) 
			   << "Opsim_rotskypos " << pSGParams->cameraRotationDeg 
			   << std::endl;

  // *****************************************************************
  // Also add in here Opsim_rottelpos which get turned into 'spiderangle'
  // by phosim.py
  // ****************************************************************
  instanceFile << "Opsim_rottelpos  0.0" << std::endl;

  // ****************************************************************
  // And now the defaults. 
  // ***************************************************************

  // ********************************
  // Moon option
  // ********************************
  double moonRA2000Rad;
  double moonDec2000Rad;
  double sunRA2000Rad;
  double sunDec2000Rad;
  if (pSGParams->moonOption == "Realistic" ) {
	// *************************************************************
	// Using the SunMoon class get the moon location for this MJD
	// And save to instance file.
	// *************************************************************
	SunMoon* pSunMoon        = new SunMoon();
	// ***************************************
	// This call gets this mjd's  sun and moon locations and returns the moon 
	// phase as a percentage of illumination
	// ***************************************
	pSunMoon->Sun2000( pSGParams->mjd, &sunRA2000Rad,  &sunDec2000Rad);
	pSunMoon->Moon2000(pSGParams->mjd, &moonRA2000Rad, &moonDec2000Rad);
	pSGParams->moonphase = 
	                      pSunMoon->MoonPhase(pSGParams->mjd,&sunRA2000Rad, 
											&sunDec2000Rad, &moonRA2000Rad, 
											&moonDec2000Rad) * 100.0;
	pSGParams->moonra    = moonRA2000Rad * 180.0/PAL__DPI;
	pSGParams->moondec   = moonDec2000Rad * 180.0/PAL__DPI;

	std::cout<<"Moon Phase: " << pSGParams->moonphase <<std::endl;

	double azRad;
	double elevRad;
	pSGInstAzelradec->RADec2000ToAzEl( moonRA2000Rad, moonDec2000Rad,
									   pSGParams->mjd, azRad, elevRad);
	pSGParams->moonalt = elevRad *180.0/PAL__DPI;
		

	// *************************************************************
	// Angular seperation between our pointing and moon
	// *************************************************************
	double dSeparation_rad = 
	                   palDsep ( pSGParams->unrefractedRADeg * PAL__DPI/180., 
								 pSGParams->unrefractedDecDeg * PAL__DPI/180., 
								 moonRA2000Rad, moonDec2000Rad);
	pSGParams->dist2moon = dSeparation_rad * 180./PAL__DPI;

  }
  else {
	if (pSGParams->moonOption != "BelowHorizen") {
	  std::cout << "*Fatal-Unknown option chosen for Moon: "
				<< pSGParams->moonOption <<"  Valaid Options are: " 
				<< " \"BelowHorizen\" or \"Realistic\" " << std::endl;
	  exit(1);
	}
  }

  // *********************************************************************
  // Good moon stuff 
  instanceFile << std::fixed << std::setprecision(4)
			   << "Opsim_moondec "   << pSGParams->moondec << std::endl;
  instanceFile << "Opsim_moonra "    << pSGParams->moonra << std::endl;
  instanceFile << "Opsim_moonalt "   << pSGParams->moonalt << std::endl;
  instanceFile << "Opsim_dist2moon " << pSGParams->dist2moon << std::endl;
  instanceFile << std::fixed << std::setprecision(2) 
			   << "Opsim_moonphase " << pSGParams->moonphase << std::endl;
 
  // ******************************************************************
  // Now the sun
  // ******************************************************************
  if( pSGParams->sunOption == "Realistic" ) {
	double azRad;
	double elevRad;
	pSGInstAzelradec->RADec2000ToAzEl( sunRA2000Rad, sunDec2000Rad,
									   pSGParams->mjd, azRad, elevRad);
	pSGParams->sunalt = elevRad *180.0/PAL__DPI;
  }
  instanceFile << std::fixed << std::setprecision(4)
			   << "Opsim_sunalt " << pSGParams->sunalt << std::endl;

  // ************************************
  // Random seed
  // ************************************
  instanceFile << "SIM_SEED     " << pSGParams->ranseed << std::endl;


  // **********************************
  // User must do the rest himself by editing the file.
  // *********************************  
  instanceFile << "Opsim_obshistid 99999999" << std::endl;
  instanceFile << "Opsim_rawseeing 0.67" << std::endl;
  instanceFile << "SIM_MINSOURCE 1" << std::endl;
  instanceFile << "SIM_TELCONFIG 0" << std::endl;
  instanceFile << "SIM_CAMCONFIG 1" << std::endl;
 
  instanceFile << "includeobj " <<  pSGParams->fObjectCatalogFileName 
	       << std::endl;
  instanceFile.close();
  return;
}  
// **************************************************************

class CatalogObject
{
public:
  CatalogObject(StarsAndGalaxiesParameters* pSGPar);
  void InitalizeCap(double longitude, double latitude);
  void GenerateSingleRandomRaDec();
  void GenerateSingleRandomMagnitude(
			   std::vector < double >& magnitudeCumulativeProb, 
			   double limitingMagnitude);
  void GenerateGridRaDec(double raRad, double decRad);
  void WriteObjectToFile(double raRad, double decRad);
  double GetRADeg(){return RARad*180.0/PAL__DPI;};
  double GetDecDeg(){return decRad*180.0/PAL__DPI;};
  double GetMagnitude(){return magnitude;};
  void   SetSedFileName()
                 {starGridSEDFileName=pSGParams->starGridSEDFileName;return;};
  std::string GetSEDFileName(){return starGridSEDFileName;};
  void ReadXYAndGenerateRaDec(double raRad, double decRad, 
			      std::string fileName);
  void GetTelRaDecPointingFromTrkAzEl(double& raRad, double& decRad,
				      AzElRADecXY* pObjAzelradec);
  bool fDebugPrint;

protected:
  StarsAndGalaxiesParameters* pSGParams;
  AzElRADecXY* pCatObjAzelradec;
  double RARad;
  double decRad;
  double magnitude;
  double zenithMaxRange;    //used in generating random elevation.
  std::string starGridSEDFileName;
  std::vector < double > rightAscension;
  std::vector < double > declination;

  std::vector < double > starMagCumulativeProb;
  std::vector < double > galaxyMagCumulativeProb;
}; 
// ***************************************************************************

CatalogObject::CatalogObject(StarsAndGalaxiesParameters* pSGPar)
{
  pSGParams =  pSGPar;
  // Read in the magnitude table, colums are:
  // Magnitude, number of stars per sq. degree, number of galaxy 
  // per sq deg.  I found the "mag_dist" table in the phosim/tools 
  // directory. Not really sure what the table really is saying. I 
  // don't think it is what we finally want but it will work as a 
  // place holder for now.
  // ****************************************************************
 
  fDebugPrint=false;

  int mm;
  double starDensity;
  double galaxyDensity;
  std::ifstream mag;
  mag.open("mag_dist");
  if ( ! mag.is_open() ) { 
    std::cout<<"Fatal- Can not find local star magnitude file: mag_dist"
	     << std::endl;
    exit(1);
  }

  while( ! mag.eof() ) {
    mag >> mm >> starDensity >> galaxyDensity;
	if(fDebugPrint) {
	  std::cout << mm << " " << starDensity << " " <<  galaxyDensity 
				<<std::endl;
	}

    starMagCumulativeProb.push_back( starDensity);
    galaxyMagCumulativeProb.push_back( galaxyDensity);
  }
  // *****************************************
  // Now normalize this, convert to a cululative probablity 
  // distribution in luminosity up to the limiting magnitude
  // *****************************************
  double starMagSum=0;
  double galaxyMagSum=0.0;
  for (int i = 0; i < pSGParams->rndmStarsLimitingMagnitude + 1; i++) {
    starMagSum += starMagCumulativeProb.at( i );
    galaxyMagSum += galaxyMagCumulativeProb.at( i );
    if (i != 0){
      starMagCumulativeProb.at(i) =  starMagCumulativeProb.at(i) + 
	starMagCumulativeProb.at(i-1);
      galaxyMagCumulativeProb.at(i) = galaxyMagCumulativeProb.at(i)
	+ galaxyMagCumulativeProb.at(i-1);
    }
  }
  for (int i = 0; i < pSGParams->rndmStarsLimitingMagnitude + 1; i++) {
    starMagCumulativeProb.at(i) = starMagCumulativeProb.at(i) / 
                                                         starMagSum;
    galaxyMagCumulativeProb.at(i) = galaxyMagCumulativeProb.at(i) 
                                                     / galaxyMagSum;
  }
 }
// ******************************************************************

void CatalogObject::InitalizeCap(double capLongRad, 
				 double capLatRad) 
{
  // ******************************************
  //Locate cap at zenith and set max size of cap
  // ************************************  
  zenithMaxRange = ( 1. - cos( ( pSGParams->rndmStarsFOVDiameterDeg / 2. )
			       * PAL__DPI / 180.0 ) ) / 2.0;
  // ****************************************************************
  // Find what ra latitude=0 and longitude=0 is at zenith (we know 
  // dec is 0)
  // ****************************************************************
  pCatObjAzelradec = new AzElRADecXY( 0.0, 0.0 );
  double originRA;
  double originDec;
  double az=0.0;
  double el=PAL__DPI/2.0;
  pCatObjAzelradec->AzEl2RADec2000( az, el, pSGParams->mjd, originRA, 
				   originDec );
  // Move origin so that zenith is  at capLongRad and capLatRad
  pCatObjAzelradec->SetLocation( capLongRad-originRA, capLatRad); 
  return;  
}
// ******************************************************************

void CatalogObject::GenerateSingleRandomRaDec()
{
  // ****************************************************************
  // We have to be a little clever in generating the random 
  // palcement. 
  // From:   // http://mathworld.wolfram.com/SpherePointPicking.html
  // 1: Pick randomly in an alt/az over a spherical cap cenetered at
  //  zenith. alt=90,az=0,0. (happens in CatalogObject constructor).
  // 2:Use the pCatObjAzelradec::SetLocation method to relocate this
  //  az/el cap center to the requested ra/dec at zenith.
  // 3:Use the conversion program to convert the az/el to the 
  // correct ra/dec
  // ****************************************************************
  // Gen random az,el in cap with diameter sgparms.rndmStarsFOVDiameterDeg
  // ***************************************
  double azimuthRad=RngDouble() * 2 * PAL__DPI;  //Uniform in azimuth
                                 //zenith 0 to zenithMaxRange in cap
  double zenithRad=acos( 1. - 2. * RngDouble() * zenithMaxRange);
  double elevationRad = PAL__DPI / 2. - zenithRad;
  pCatObjAzelradec->AzEl2RADec2000( azimuthRad, elevationRad, 
				    pSGParams->mjd, RARad,  decRad );
  return;
}
// ****************************************************************
  
 void CatalogObject::GenerateSingleRandomMagnitude(
			   std::vector < double >& magnitudeCumulativeProb, 
			   double limitingMagnitude)
// *****************************************************************
// Search back through the cumulative table and then interpolate
// *****************************************************************
{
  double r = RngDouble();
  while ( r <= magnitudeCumulativeProb.at( 0 ) ) { 
    r = RngDouble(); //Lower limit (highest bightness) is mag 0
  } 
  magnitude = 0.0;
                      
  for( int i = 1; i < limitingMagnitude + 1; i++) {
       if (r <= magnitudeCumulativeProb.at( i ) ) {
	// This gets us between which magnitudes we are. Assume that 
	// magnitude is evenly  distributed between the adjacent M,
	// which obviously it is not.
	 magnitude = (double)i;   
	 r = RngDouble();
	 magnitude = r + i - 1;
	 break;
       }
  }
  return;
}
// *****************************************************************

void CatalogObject::ReadXYAndGenerateRaDec(double raRad, double decRad,
					   std::string fileName)
// **********************************************************************
// Read a list of X Y coordinates(in deg)in the focal plane from a text file
// and convert to  Ra Dec for catalog. Tel pointing at raRad,decRad
// ****************************************************************
// Signs of directions will be determined later. Just go with 
// "whatever" for now.
// ****************************************************************
// Make up a vector of vectors of focal plane locations. 
// Outermost is "Y" which is along altitude coord.
// ****************************************************************
{
  pCatObjAzelradec = new AzElRADecXY( 
			   pSGParams->eastLongitudeDeg * PAL__DPI / 180.0, 
			   pSGParams->latitudeDeg * PAL__DPI / 180.0 );
  rightAscension.clear();
  declination.clear();

  // ************************************************************
  // Open the file and read in the x,y pairs, convert to ra/dec
  // ************************************************************
  std::ifstream stream1(fileName.c_str());
  if(!stream1){
    std::cout << "Fatal--While trying to open "<<fileName
	      << " an error is encountered" << std::endl;
    exit(1);
    
  }
  double xPointDeg;
  double yPointDeg;
  double pointRA;
  double pointDec;
  stream1 >> xPointDeg >> yPointDeg;
  while(!stream1.eof()) {
    //std::cout<<xPointDeg <<" "<< yPointDeg<<std::endl;
    pCatObjAzelradec->XY2RADec2000( xPointDeg, yPointDeg, 
				    pSGParams->mjd, raRad, decRad,
				    pointRA, pointDec);
    
    rightAscension.push_back(pointRA);
    declination.push_back(pointDec);
    stream1 >> xPointDeg >> yPointDeg;
}
  return;
}
// ****************************************************************


void CatalogObject::GenerateGridRaDec(double raRad, double decRad)
// *****************************************************************
// Generates a square grid of focalplane locations(in deg) and 
// converts to equivalent ( tangent plane or gnomonic) projection to
// the celestial sphere in ra/dec centered at the specified ra/dec
// *****************************************************************
// Signs of directions will be determined later. Just go with 
// "whatever" for now.
// ****************************************************************
// Make up a vector of vectors grid of focal plane locations. 
// Outermost is "Y" which is along altitude coord.
// ****************************************************************
{
  bool debugPrint = false;
  pCatObjAzelradec = new AzElRADecXY( 
			   pSGParams->eastLongitudeDeg * PAL__DPI / 180.0, 
			   pSGParams->latitudeDeg * PAL__DPI / 180.0 );
  rightAscension.clear();
  declination.clear();

  // *******************************
  // Fill X;
  // *******************************
  
  int npoints =  pSGParams->starGridWidthDeg / pSGParams->starGridSpacingDeg;
  // Since we want a point at the very center this should be odd
  if (npoints%2 == 0) {
    npoints=npoints+1;
  }
  std::cout << " #Star Grid spacing(deg):" << pSGParams->starGridSpacingDeg 
			<< std::endl;
  std::cout << " #Number of points across grid: " << npoints << std::endl;
  std::cout << " #Number of points in grid:     " << npoints*npoints 
			<< std::endl;
  
  if (npoints*npoints > kListNumberCount) {
	std::cout << " This may take a few minutes. Be patinet!" << std::endl;
	std::cout <<" Grid Ra/Dec creation progress: each # = " << kListNumberCount 
			  <<" grid points" << std::endl;
	std::cout<< " " << std::flush; //Start off a little offset to lineup with above 
  }

  double halfWidth=( ( npoints - 1 ) /2 ) *  pSGParams->starGridSpacingDeg;

  for ( int j = 0; j < npoints; j++ ) {

    double yPointDeg= (- halfWidth) + j * pSGParams->starGridSpacingDeg;

    for ( int i = 0 ;i < npoints ; i++ ) {

      double xPointDeg= (- halfWidth) + i * pSGParams->starGridSpacingDeg;

      //Convert to ra/dec
      double pointRA;
      double pointDec;
	  if (debugPrint) {
		std::cout << "xPointDeg, yPointDeg: "<<  xPointDeg << " " << yPointDeg
				  << std::endl;
	  }


      pCatObjAzelradec->XY2RADec2000( xPointDeg, yPointDeg, 
									  pSGParams->mjd, raRad, decRad,
									  pointRA, pointDec);
      
      rightAscension.push_back(pointRA);
      declination.push_back(pointDec);
	  if (debugPrint) {
		std::cout << "at point: " <<  rightAscension.size() 
				  << ": pointRA,pointDec: "<<  pointRA << " " << pointDec
				  << std::endl;
	  }

	  if ( rightAscension.size()%kListNumberCount == 0) {
		std::cout<< "#" << std::flush;
	    if  ( rightAscension.size()%(kListNumberCount*10) == 0) {
		  std::cout<< "|" << std::flush;
		}
	  }
    }
  }
  std::cout << std::endl;
  return;
}
// ****************************************************************

void CatalogObject::GetTelRaDecPointingFromTrkAzEl(double& raRad, 
												   double& decRad,
												   AzElRADecXY* pObjAzelradec)
{
  pObjAzelradec->SetLocation( 
			     pSGParams->eastLongitudeDeg * PAL__DPI / 180.0, 
			     pSGParams->latitudeDeg * PAL__DPI / 180.0 ); 
  pObjAzelradec->AzEl2RADec2000( 
				pSGParams->trkAzimuthDeg * PAL__DPI / 180., 
				pSGParams->trkAltitudeDeg * PAL__DPI / 180, 
				pSGParams->mjd, raRad, decRad);
  return;
}
// *****************************************************************




class StarObject :public CatalogObject
// **********************************************************
// Note this class derived from CatalogObject and thus has 
// CatalogObject's methods
// **************************************************
{
 public:
  StarObject(StarsAndGalaxiesParameters* pSGPar, 
	     std::ofstream& objFile);
  void GenerateRandomStars();
  void GenerateGridStars();
  void GenerateRaDecFromStarXYMFile(std::string fileName);
 
private:
  StarsAndGalaxiesParameters* pSGParams;
  std::ofstream* os;
  AzElRADecXY* pSObjAzelradec;
};
// *****************************************************************


StarObject::StarObject(StarsAndGalaxiesParameters* pSGPar, 
		       std::ofstream& objFile):
    CatalogObject(pSGPar)  

{
  pSGParams = pSGPar;
  os = &objFile;
  pSObjAzelradec = new AzElRADecXY(pSGParams->eastLongitudeDeg, 
				   pSGParams->latitudeDeg); 
}

void StarObject::GenerateRandomStars()
// *****************************************************************
// Generate star objects randomly over our fov and append them to
// the object file. 
// Be careful  with the random placement so that the local density 
// at all points in the FOV is the same.
// Use the file 'mag_dist' to get the distribution of star magnitudes.
// Use flat sed.
// (We need to  improve upon this with a good color Magnitude/galactic latude
// function from some star count program)
// ******************************************************************
{
  // ***************************************************************
  // Set the location to latitude to be the declination, and 
  // the longitude to  be the ra.
  // ****************************************************************
  // This is a trick to place the az/el cap at zenith.
  // double longRad = raRad;
  // double latRad  = decRad;
  double longRad = pSGParams->unrefractedRADeg * PAL__DPI / 180.0;
  double latRad  = pSGParams->unrefractedDecDeg * PAL__DPI / 180.0;
  InitalizeCap(longRad,latRad);  //CatalogObject method

  // *************************** 
  // Now make the objects
  // ***************************
  for (int i=0; i<pSGParams->rndmStarsNumber; i++) {
    GenerateSingleRandomRaDec();
    GenerateSingleRandomMagnitude(starMagCumulativeProb, 
				  pSGParams->rndmStarsLimitingMagnitude );
    SetSedFileName();
    pSGParams->catalogObjectID++;
    // Now write the star object to the catalog file
    *os<< "object " << pSGParams->catalogObjectID << " " << std::fixed 
       << std::setprecision(8) << GetRADeg()<< " " << GetDecDeg() 
       << "  " << std::setprecision(6) << GetMagnitude() << " " 
       << GetSEDFileName() << " 0.0 0.0 0.0 0.0 0.0 0.0 star none  none" 
       << std::endl;

    //std::cout<< GetMagnitude() << " " << GetRADeg() << " "
    //	     << GetDecDeg() << std::endl;
  }
}
// ************************************************************

void StarObject::GenerateGridStars()
{
  // ***************************************************************
  //Get Ra/Dec of where the tel is pointing
  // ***************************************************************
  double raRad   = pSGParams->unrefractedRADeg * PAL__DPI / 180.0;
  double decRad  = pSGParams->unrefractedDecDeg * PAL__DPI / 180.0;
  GenerateGridRaDec(raRad, decRad);    //CatalogObject method:

  int numRaDecPoint=rightAscension.size();
  std::cout<<" #Number of new star grid points in Catalog file: " 
	   << numRaDecPoint << std::endl;

  // Now write starobject to catalog file
  for ( int i = 0; i < numRaDecPoint; i++ ) {
    pSGParams->catalogObjectID++;
    *os<< "object " << pSGParams->catalogObjectID << " " << std::fixed 
       << std::setprecision(8) << rightAscension.at(i) * 180 / PAL__DPI 
       << " " << declination.at(i) * 180 / PAL__DPI 
       << "  " << std::setprecision(6) << pSGParams->starGridFixedMagnitude
       << " "  << pSGParams->starGridSEDFileName 
       << " 0.0 0.0 0.0 0.0 0.0 0.0 star none none" 
       << std::endl;

    //std::cout<< pSGParams->starGridFixedMagnitude << " " 
    //	     << rightAscension.at(i) * 180. / PAL__DPI << " "
    //	     << declination.at(i) * 180. / PAL__DPI << std::endl;
  }
  return;
}
// ****************************************************************
void StarObject::GenerateRaDecFromStarXYMFile(std::string fileName)
{
  // ***************************************************************
  //Get Ra/Dec of where the tel is pointing
  // ***************************************************************
  double raRad   = pSGParams->unrefractedRADeg * PAL__DPI / 180.0;
  double decRad  = pSGParams->unrefractedDecDeg * PAL__DPI / 180.0;
  ReadXYAndGenerateRaDec(raRad, decRad, fileName);   //CatalogObject method:

  int numRaDecPoint=rightAscension.size();
  std::cout<<" #Number of XY points to add to Catalog file: " 
	   << numRaDecPoint << std::endl;

  // Now write starobject to catalog file
  for ( int i = 0; i < numRaDecPoint; i++ ) {
    pSGParams->catalogObjectID++;
    *os<< "object " << pSGParams->catalogObjectID  << " " << std::fixed 
       << std::setprecision(8) << rightAscension.at(i) * 180 / PAL__DPI 
       << " " << declination.at(i) * 180 / PAL__DPI 
       << "  " << std::setprecision(6) << pSGParams->starGridFixedMagnitude
       << " "  << pSGParams->starGridSEDFileName 
       << " 0.0 0.0 0.0 0.0 0.0 0.0 star none none" 
       << std::endl;

    //std::cout<< pSGParams->starGridFixedMagnitude << " " 
    //	     << rightAscension.at(i) * 180. / PAL__DPI << " "
    //	     << declination.at(i) * 180. / PAL__DPI << std::endl;
  }
  return;
}
// ****************************************************************

class GalaxyObject: public CatalogObject
{
 public:
  
  GalaxyObject(StarsAndGalaxiesParameters* pSGPar, 
	       std::ofstream& objFile);
  void GenerateRandomGalaxies();
  void GenerateGridGalaxies();
  void GenerateAllGalacticusObjects();
  void GenerateAllBuzzardObjects();

private:
  std::string GetGalaxySedFileName();

  StarsAndGalaxiesParameters* pSGParams;
  std::ofstream* os;
  AzElRADecXY* pGalObjAzelradec;

};

GalaxyObject::GalaxyObject(StarsAndGalaxiesParameters* pSGPar, 
			   std::ofstream& objFile):
    CatalogObject( pSGPar )
{
  pSGParams = pSGPar;
  os = &objFile;
  // **************************************************
  // Load up the checklist for the Galacticus catalog.
  // This is a list of all the elements in the galacticus entry that we use 
  // that must have a value and not be nan or -inf or inf.
  //  *************************************************
  SGGalacticusLSSTFile GalacticusLSSTFile;
  GalacticusLSSTFile.AddElementToCheckList(GE_DISK_LSST_G_OBSERVED);
  GalacticusLSSTFile.AddElementToCheckList(GE_BULGE_LSST_G_OBSERVED);
  GalacticusLSSTFile.AddElementToCheckList(GE_DISK_RA);
  GalacticusLSSTFile.AddElementToCheckList(GE_DISK_DEC);
  GalacticusLSSTFile.AddElementToCheckList(GE_BULGE_RA);
  GalacticusLSSTFile.AddElementToCheckList(GE_BULGE_DEC);
  GalacticusLSSTFile.AddElementToCheckList(GE_REDSHIFT);
  GalacticusLSSTFile.AddElementToCheckList(GE_BULGE_RE);
  GalacticusLSSTFile.AddElementToCheckList(GE_BULGE_PHI);
  GalacticusLSSTFile.AddElementToCheckList(GE_BULGE_INDEX);
  GalacticusLSSTFile.AddElementToCheckList(GE_DISK_RE);
  GalacticusLSSTFile.AddElementToCheckList(GE_DISK_PHI);
  GalacticusLSSTFile.AddElementToCheckList(GE_DISK_INDEX);
  GalacticusLSSTFile.AddElementToCheckList(GE_DISK_THETA_LOS);
}
// ****************************************************************

void GalaxyObject::GenerateRandomGalaxies()
// ******************************************************
// Generate galaxy objects randomly over the FOV. 
// ******************************************************
// *****************************************************************
// Generate star objects randomly over our fov and append them to
// the object file. 
// Be careful  with the random placement so that the local density 
  // at all points in the FOV is the same.
// Use the file mag_dist to get the distribution of star magnitudes.
// Use flat sed.
// ******************************************************************
{
  // ***************************************************************
  // Set the location to latitude to be the declination, and 
  // the longitude to  be the ra.
  // ****************************************************************
  //  double raRad;
  //double decRad;

  // ***************************************************************
  //Get Ra/Dec of where the tel is pointing
  // ***************************************************************
  //  GetTelRaDecPointingFromTrkAzEl(raRad, decRad, pGalObjAzelradec);

  // This is a trick to place the az/el cap at zenith.
  //double longRad = raRad;
  //double latRad  = decRad;

  double longRad   = pSGParams->unrefractedRADeg * PAL__DPI / 180.0;
  double latRad  = pSGParams->unrefractedDecDeg * PAL__DPI / 180.0;
  InitalizeCap(longRad,latRad);  //CatalogObject method

  // *************************** 
  // Now make the objects
  // ***************************
  for (int i=0; i<pSGParams->rndmStarsNumber; i++) {
    GenerateSingleRandomRaDec();
    GenerateSingleRandomMagnitude(galaxyMagCumulativeProb, 
				  pSGParams->rndmGalaxyLimitingMagnitude );
    SetSedFileName();  //may need 2? see below

    // *****************************************************************
    // Each galaxy will be made up of 2 objects in the catalog: the bulge and 
    // the disk. We will use the sersic2D specification
    // Bulge will have sersic index of 4 and disk will have sersic index of 1
    // *****************************************************************
    std::vector < double > bulgeSersic(4);
    std::vector < double > diskSersic(4);
    bulgeSersic.at(3)=4.0;       //Sersic index.
    diskSersic.at(3)=1.0;

    // *********************************************************************
    // For this toy model of galazies, have position angle of disk and bulge 
    // be random and uncorrelated
    // *****************************************************************
    bulgeSersic.at(2) = RngDouble() * 360.0;  //Sersic Position Angle in Deg
    diskSersic.at(2) = RngDouble() * 360.0;
    
    // *****************************************************************
    // We need to divide the magnitude between the bulge and disk.
    // from:  
    //  http://www.imprs-astro.mpg.de/sites/default/files/2004_Tasca_Lidia.pdf
    // from page 51. B/T Bulge flux to total flux=
    // (Sersic + exponential)(iband (55.41 � 1.98)%)
    // ******************************************************************
    //double bulgeToTotalFlux = kBulgeToTotalFlux + 
	//                             kBulgeToTotalFluxSigma * random_gaussian();
    //double bulgeMagnitudeDifference = -2.5 * log10( bulgeToTotalFlux );
    //double diskMagnitudeDifference = -2.5 * log10( 1. - bulgeToTotalFlux );

    //double bulgeMagnitude = GetMagnitude() + bulgeMagnitudeDifference;
    //double diskMagnitude  = GetMagnitude() + diskMagnitudeDifference;

    pSGParams->catalogObjectID++;
    // Now write starobject to catalog file
    *os<< "object " << pSGParams->catalogObjectID << " " << std::fixed 
       << std::setprecision(8) << GetRADeg()<< " " << GetDecDeg() 
       << "  " << std::setprecision(6)<<GetMagnitude()
       << " " << GetSEDFileName() << " 0.0 0.0 0.0 0.0 0.0 0.0 star none none"
       << std::endl;

    //std::cout<< GetMagnitude() << " " << GetRADeg() << " "
    //	     << GetDecDeg() << std::endl;
  }
}
// ****************************************************************

void  GalaxyObject::GenerateGridGalaxies()
{
  //not yet
}
// ****************************************************************

void GalaxyObject::GenerateAllGalacticusObjects()
// **************************************************************************
// Read in all the Galacticus galaxies and convert to phsim objects
// Save in Object file. 
// This is a way to generate a phosim compatable Galacticus object file
// **************************************************************************
// Note each galacticus galaxy will be converted to 2 phosim opjects: a 
// bulge object and a seperate disk object. We can use the object ID's 
// with .1 and .2 extentions to indicate thay belong together
// **************************************************************************
{
  SGGalacticusLSSTFile GalacticusLSSTFile;
  
  std::cout<< "Opening Galacticus input text file: "
	   << pSGParams->GalacticusLSSTFileName<<std::endl;
   GalacticusLSSTFile.OpenGalacticusTextFile(pSGParams->GalacticusLSSTFileName);
  
  // Start reading in the galacticus objects
  std::vector< double >  galaxyData;
  std::string GalacticusBulgeSEDFileName;
  std::string GalacticusDiskSEDFileName;
  while(1) {
    bool gotRecord=GalacticusLSSTFile.ReadGalacticusTextFile(galaxyData, 
					     GalacticusBulgeSEDFileName,
					     GalacticusDiskSEDFileName );
    if (! gotRecord) {
      GalacticusLSSTFile.CloseGalacticusTextFile();
      break;
    }
    // *****************************************************
    // Make a pair of galaxy objects, bulge and disk.  Use index like 33.1 
    // and 33.2
    // *****************************************************
   pSGParams->catalogObjectID++;
   // ********************************************************
   //Programming Note: We are using the "SGGalacticusElements" enum (see 
   // sg_lsst_galacticus_file.h) to find things in galaxyData
   // *********************************************************
 

   // ***********************************************************
   // We don't have the "proper" AB mag at 500nm as required by phosim
   // so we use the g filter values instead, though for high z we should 
   // probably be using the U filter values. This all need quite a bit of 
   // work to get right. So we fake it for now
   // ***********************************************************
   double diskABMagnitude  = galaxyData.at(GE_DISK_LSST_G_OBSERVED);
   double bulgeABMagnitude = galaxyData.at(GE_BULGE_LSST_G_OBSERVED);


   double diskRADeg   = galaxyData.at(GE_DISK_RA);//For now just use the
                                                  //RA/Dec in the file. We can
                                                  //play with it later if we 
                                                  //want to.
   double diskDecDeg  = galaxyData.at(GE_DISK_DEC);
   double bulgeRADeg  = galaxyData.at(GE_BULGE_RA);
   double bulgeDecDeg = galaxyData.at(GE_BULGE_DEC);
   double   redshift  = galaxyData.at(GE_REDSHIFT);

   std::string bulgeSEDFileName;
   std::string diskSEDFileName;
   if (GalacticusLSSTFile.hasSED()) {
     bulgeSEDFileName = GalacticusBulgeSEDFileName;
     diskSEDFileName  = GalacticusDiskSEDFileName;
   }
   else {
     bulgeSEDFileName = GetGalaxySedFileName();
     diskSEDFileName  = GetGalaxySedFileName();
   }

 
   // *************************************************************
   // We will use the sersig2D shape model;ing for the bulge and the disk
   // of our galaxies.
   // *************************************************************
   // *************************************************************
   // Bulge has no orientation: 
   // *************************************************************
   double bulgeMajorHalfLightRadiusArcSec = galaxyData.at( GE_BULGE_RE );
   double bulgeMinorHalfLightRadiusArcSec = bulgeMajorHalfLightRadiusArcSec; 
   double bulgePositionDeg   = galaxyData.at( GE_BULGE_PHI );
   //double bulgePositionRad   = bulgePositionDeg * ( PAL__DPI / 180.);
   int    bulgeSersicIndex                = 4;// galaxyData.at(GE_BULGE_INDEX);
                                                               
   double diskMajorHalfLightRadiusArcSec  = galaxyData.at( GE_DISK_RE );
   double diskMinorHalfLightRadiusArcSec  = diskMajorHalfLightRadiusArcSec 
                * cos( galaxyData.at(GE_DISK_THETA_LOS) * ( PAL__DPI / 180.) );
   double diskPositionDeg     = galaxyData.at( GE_DISK_PHI );
   //double diskPositionRad     = diskPositionDeg * ( PAL__DPI / 180.);
   int    diskSersicIndex                 = 1;// galaxyData.at(GE_DISK_INDEX);

   // Now write galaxy bulge object to the catalog file
    *os<< "object " << pSGParams->catalogObjectID << ".1 " << std::fixed 
       << std::setprecision(8) << bulgeRADeg << " " << bulgeDecDeg 
       << "  " << std::setprecision(6) << bulgeABMagnitude << " " 
       <<  bulgeSEDFileName << " " << std::setprecision(6) << redshift
       << " 0.0 0.0 0.0 0.0 0.0 sersic2D " <<  bulgeMajorHalfLightRadiusArcSec
       << " " << bulgeMinorHalfLightRadiusArcSec << " " 
	  //<< bulgePositionRad
	   << bulgePositionDeg
       << " " << bulgeSersicIndex << "  none none "
       << std::endl;

    // Now write galaxy disk object to the catalog file
    *os<< "object " << pSGParams->catalogObjectID << ".2 " << std::fixed 
       << std::setprecision(8) << diskRADeg << " " << diskDecDeg 
       << "  " << std::setprecision(6) << diskABMagnitude << " " 
       <<  diskSEDFileName << " " << std::setprecision(6) << redshift
       <<" 0.0 0.0 0.0 0.0 0.0 sersic2D " << diskMajorHalfLightRadiusArcSec
       << " " << diskMinorHalfLightRadiusArcSec << " " 
	  //<< diskPositionRad
	   << diskPositionDeg
       << " " << diskSersicIndex << "  none none"
       << std::endl;
    }
  std::cout << "Galacticus LSST catalog file has " 
	    << pSGParams->catalogObjectID +1<<" galaxies." <<std::endl;
  
  return;
}
// *************************************************************************

void GalaxyObject::GenerateAllBuzzardObjects()
// **************************************************************************
// Read in  the Buzzard galaxies and convert to phsim objects
// Save in Object file. 
// This is a way to generate a phosim compatable Buzzard  object file
// **************************************************************************
// ?????Note each Buzzard galaxy will be converted to 2 phosim opjects: a 
// bulge object and a seperate disk object. We can use the object ID's 
// with .1 and .2 extentions to indicate thay belong together???
// **************************************************************************
{
  SGBuzzardLSSTFile BuzzardLSSTFile;
  
  std::cout<< "Opening Buzzard-highres Truth Fits file: "
		   << pSGParams->BuzzardTruthFileName << std::endl;

  BuzzardLSSTFile.OpenBuzzardFitsFiles(pSGParams->BuzzardTruthFileName);

  cout << "Also opened companion LSST Magnitude file: " 
		<<  BuzzardLSSTFile.GetLSSTMagFileName() << std::endl;


  // Start reading in the Buzzard galaxies
  SGBuzzardGalaxy buzzardGalaxy;
  while(1) {
    bool gotRecord=BuzzardLSSTFile.GetNextGalaxy(buzzardGalaxy);
    if (! gotRecord) {
      break;
    }

    // *****************************************************
    // Make a pair of galaxy objects, bulge and disk.  Use index like 33.1 
    // and 33.2
    // *****************************************************

   pSGParams->catalogObjectID++;

    // ***********************************************************
   // We don't have the "proper" AB mag at 500nm as required by phosim
   // so we use the g filter values instead, though for high z we should 
   // probably be using the U filter values. This all need quite a bit of 
   // work to get right. So we fake it for now
   // ***********************************************************
   double diskABMagnitude  = buzzardGalaxy.omag;
   double bulgeABMagnitude = buzzardGalaxy.omag;

   double diskRADeg   =  buzzardGalaxy.RADeg;     //For now just use the
                                                  //RA/Dec in the file. We can
                                                  //play with it later if we 
                                                  //want to.
   double diskDecDeg  = buzzardGalaxy.decDeg;
   double bulgeRADeg  = buzzardGalaxy.RADeg;
   double bulgeDecDeg = buzzardGalaxy.decDeg;
   double   redshift  = buzzardGalaxy.z;

   std::string bulgeSEDFileName = GetGalaxySedFileName();
   std::string diskSEDFileName  =  GetGalaxySedFileName();
 
   // *************************************************************
   // We will use the sersig2D shape model;ing for the bulge and the disk
   // of our galaxies.
   // *************************************************************
   // *************************************************************
   // Bulge has no orientation: 
   // *************************************************************
   //!!!!!!!!!!!!!$$$$$$$$$$$$$$$$Note buzzaardData not working right yet, 
   //!!!!!!!!!!!!!$$$$$$$$$$$$$$$$All following references are bogus until I 
   //!!!!!!!!!!!!!$$$$$$$$$$$$$$$$can figure them out.
   double bulgeMajorHalfLightRadiusArcSec = buzzardGalaxy.tsize;
   double bulgeMinorHalfLightRadiusArcSec = bulgeMajorHalfLightRadiusArcSec; 
   double bulgePositionDeg   = buzzardGalaxy.id;
   //double bulgePositionRad   = bulgePositionDeg * ( PAL__DPI / 180.);
   int    bulgeSersicIndex                = 4;// buzzardGalaxy.id;
                                                               
   double diskMajorHalfLightRadiusArcSec  = buzzardGalaxy.id;
   double diskMinorHalfLightRadiusArcSec  = diskMajorHalfLightRadiusArcSec 
                     * cos( buzzardGalaxy.id * ( PAL__DPI / 180.) );
   double diskPositionDeg     = buzzardGalaxy.id;
   //double diskPositionRad     = diskPositionDeg * ( PAL__DPI / 180.);
   int    diskSersicIndex                 = 1;// buzzardGalaxy.id;

   // Now write galaxy bulge object to the catalog file
    *os<< "object " << pSGParams->catalogObjectID << ".1 " << std::fixed 
       << std::setprecision(8) << bulgeRADeg << " " << bulgeDecDeg 
       << "  " << std::setprecision(6) << bulgeABMagnitude << " " 
       <<  bulgeSEDFileName << " " << std::setprecision(6) << redshift
       << " 0.0 0.0 0.0 0.0 0.0 sersic2D " <<  bulgeMajorHalfLightRadiusArcSec
       << " " << bulgeMinorHalfLightRadiusArcSec << " " 
	  //<< bulgePositionRad
	   << bulgePositionDeg
       << " " << bulgeSersicIndex << "  none none "
       << std::endl;

    // Now write galaxy disk object to the catalog file
    *os<< "object " << pSGParams->catalogObjectID << ".2 " << std::fixed 
       << std::setprecision(8) << diskRADeg << " " << diskDecDeg 
       << "  " << std::setprecision(6) << diskABMagnitude << " " 
       <<  diskSEDFileName << " " << std::setprecision(6) << redshift
       <<" 0.0 0.0 0.0 0.0 0.0 sersic2D " << diskMajorHalfLightRadiusArcSec
       << " " << diskMinorHalfLightRadiusArcSec << " " 
	  //<< diskPositionRad
	   << diskPositionDeg
       << " " << diskSersicIndex << "  none none"
       << std::endl;
    }
  std::cout << "Galacticus LSST catalog file has " 
	    << pSGParams->catalogObjectID +1<<" galaxies." <<std::endl;
  
  return;
}
// *************************************************************************

std::string GalaxyObject::GetGalaxySedFileName()
{
  std::string SEDFileName =  pSGParams->starGridSEDFileName;
  return SEDFileName;
}
// **************************************************************************

double GetLastObjectID(std::ifstream& inCatFile)
// **************************************************************************
// Search the inCsarFile for the lats line which starts with the string Object
// Second string in that line will be the last object ID. Return the integer part 
// of that.
// **************************************************************************
{
  // **********************************************************************
  // Do this the dumb slow stupid way that is gaurenteed to work. Just read in the 
  // whole file line by line until we reach the eof (or a line that doesn't start
  // with the string  "object".
  // **********************************************************************
  std::string catLine;
  double catLastObjectID;
  std::istringstream catIss;
  std::string catObj;
  double catFullObjectID;

  catLastObjectID = 0;

  while (getline (inCatFile,catLine))
	{
	  if ( inCatFile.eof() ) {
		return catLastObjectID;
	  }
	  else{
		catIss.str(catLine);
		catIss >> catObj;
		if (catObj != "object") {   //test this line is good
		  return catLastObjectID;
		}
		else{
		  catIss >> catFullObjectID;
		  catLastObjectID =  catFullObjectID;  // Truncate to get integer part
		}
	  }
	}
  return catLastObjectID;
}
// **************************************************************************


int main(int argc, char* argv[])
{
  
  // ***********************************************************************
  // For input to this program we are using a .pars file. The file name can
  // be specified on the command line with a "commands.pars" like command.
  // Also allowed is a "-h" request to print a short explination of the 
  // primary commands avaiable to put in the commands.pars file.
  // A "-u" (for usage) option is avaiable tfor a detailed listing and 
  // explination of all commands.
  // ***********************************************************************

  // ***********************************************************
  // Get name of input parameter file from command line, or execute "help"
  // (-h) or if no argurments do help and usage
  // ***********************************************************
  StarsAndGalaxiesParameters* pSGParams = 
	                                 new StarsAndGalaxiesParameters(argc,argv);
  std::string inputFileName;

  if ( pSGParams->IsHelpRequest() ) {
	pSGParams->Help();
	exit(0);
  }
  else{ 
	if ( pSGParams->IsUsageRequest() ) {
	  pSGParams->Help();
	  pSGParams->Usage();
	  exit(0);
	}
	else{
	  
	  inputFileName=pSGParams->GetInputFileName();
	}
  }

  readText pars(inputFileName);

  // ***************************************************************
  // Read in the User configuration 
  // parameters
  // ***************************************************************
  
  pSGParams->ReadParameters(pars);
 
  // ***************************************************************
  // Create instance and object catalog files
  // May want to test for sucess here???
  // Note: The Instance file is always new (i.e. replace (ios:trunc) any 
  // existing file or make a new one)
  // ***************************************************************
  std::ofstream instanceFile( pSGParams->fInstanceFileName.c_str(), 
							  std::ios::out | std::ios::trunc);
  
  // ****************************************************************
  // Make the instance file
  // ****************************************************************
  StarsAndGalaxiesInstanceFile sgInstance;
  sgInstance.CreateInstanceFile(instanceFile,pSGParams);
  
  // **************************************************************
  // Create (or add too)the catalog file 
  // Note we open the catalog file for appending
  // **************************************************************
  // We need to be careful about catlog object Id's. The inital value for this 
  // run will be the last one in an exitsing file +1 or 0 if this is a new 
  // file. So if we have an existing file, open it for input, get the last 
  // line, get the catalog id.
  // **************************************************************

  std::ifstream inObjectFile( pSGParams->fObjectCatalogFileName.c_str());
  if(inObjectFile){
	// **********************************************************************
	// File exists , go to end of file. Search back to beginning of last line
	// Get object id.
	// **********************************************************************
	pSGParams->catalogObjectID = GetLastObjectID(inObjectFile); //round down to int
	
	cout<< "Last Object ID base in Original Catalog file: " 
		<< pSGParams->catalogObjectID << std::endl; 
	inObjectFile.close();
  }
  else {
	std::cout << "Creating New Catalog Objectfile: " 
			  << pSGParams->fObjectCatalogFileName << std::endl;
  } 
  std::ofstream objectFile( pSGParams->fObjectCatalogFileName.c_str(), 
                                               std::ios::out | std::ios::app ); 

  if (pSGParams->IsRandomStars() || pSGParams->IsStarGrid() || 
	  pSGParams->IsCustomStars() ) {
	//	std::cout<<"Generating any requested Star objects"<<std::endl;
	StarObject starObj(pSGParams, objectFile);

	// **************************************************************
	// Do star catalogs. Random distribution in sky first
	// **************************************************************
	if( pSGParams->IsRandomStars() ) {
	  std::cout << "**Generating " << pSGParams->rndmStarsNumber 
				<< " random stars." << std::endl;
	  starObj.GenerateRandomStars();
	}
	
	// This gets appended to catalog (so other objects may already be there).
	if ( pSGParams->IsCustomStars() ) {
	  std::cout << "Reading Custom Stars from" << pSGParams->starXYMFile 
				<<std::endl;
	  starObj.GenerateRaDecFromStarXYMFile(pSGParams->starXYMFile); 
	}

	// Grid of stars and/or galaxy is next. All have same magnitiude
	// This gets added to catalog (so random may already be there).
	if( pSGParams->IsStarGrid() ) {
	  std::cout << "**Generating Grid of Stars" << std::endl; 
	  starObj.GenerateGridStars();
    }
  }
  // **************************************************************
  // Do galaxy catalogs.
  // **************************************************************
  if( pSGParams->IsRandomGalaxies() || pSGParams->IsGalaxyGrid() ||
	  pSGParams->GalacticusLSSTFileName != " ") {	
	GalaxyObject galaxyObj(pSGParams, objectFile);
	
	// This gets added to catalog (so stars may already be there).
	if ( pSGParams->IsRandomGalaxies() ) {
	  std::cout << "Generating " << pSGParams->rndmGalaxiesNumber 
				<< " random Galaxies."<<std::endl;
	  galaxyObj.GenerateRandomGalaxies(); 
	}
	
	// This gets added to catalog (so stars may already be there).
	if ( pSGParams->IsGalaxyGrid() ) {
	  galaxyObj.GenerateGridGalaxies();
	}
  
	// **************************************************************
	// This is a probably one- time run
	// Read in from the Galacticus text file of galaxies and create
	// a phosim galaxy object file from it. 
	// All other galaxy catalog will be generated from this object file.
	// **************************************************************
	if ( pSGParams->GalacticusLSSTFileName != " " ) {
	  galaxyObj.GenerateAllGalacticusObjects();
	}
  }
  // ***************************************************
  // and we are done
  // ***************************************************
  inObjectFile.open( pSGParams->fObjectCatalogFileName.c_str());
  int lastObjectID = GetLastObjectID(inObjectFile); //round down to int
  cout<< "Final Object ID base in Catalog file: " 
	  << lastObjectID << std::endl; 
  inObjectFile.close();

  std::cout << "#StarAndGalaxies: Done!" << std::endl;
  return 0;
}  
// ************************************************************************
